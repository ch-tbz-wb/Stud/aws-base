# Modul 2 - Knowledge Check

### 1. Frage

For certain services like Amazon Elastic Compute Cloud (Amazon EC2) and Amazon Relational Database Service (Amazon RDS), you can invest in reserved capacity. What options are available for Reserved Instances?

- [ ] AURI
- [ ] MURI
- [ ] NURI
- [ ] PURI
- [ ] DURI

----

### 2. Frage

Where can a customer go to get more details about Amazon Elastic Compute Cloud (Amazon EC2) billing activity that took place 3 months ago? (Select the best answer.)

- [ ] Amazon EC2 dashboard
- [ ] AWS Cost Explorer
- [ ] AWS Trusted Advisor dashboard
- [ ] AWS CloudTrail logs stored in Amazon Simple Storage Service (Amazon S3)

----

### 3. Frage

True or false? To receive the discounted rate associated with Reserved Instances, you must make a full, upfront payment for the term of the agreement. (Select the best answer.)

- [ ] True
- [ ] False 

----

### 4. Frage

Which statement is true about the pricing model on AWS? (Select the best answer.)

- [ ] In most cases, there is a per gigabyte charge for inbound data transfer
- [ ] Storage is typically charged per gigabyte
- [ ] Compute is typically charged as a monthly fee based on instance type
- [ ] Outbound charges are free up to a per account limit

----

### 5. Frage

What are the four support plans offered by AWS Support? (Select the best answer.)

- [ ] Basic, Developer, Business, Enterprise
- [ ] Basic, Startup, Business, Enterprise
- [ ] Free, Bronze, Silver, Gold
- [ ] All support is free


----

### 6. Frage

What AWS tool lets you explore AWS services and create an estimate for the cost of your use cases on AWS? (Select the best answer.)

- [ ] AWS Pricing Calculator
- [ ] AWS Budgets
- [ ] AWS Cost and Usage Report
- [ ] AWS Billing Dashboard

----

### 7. Frage

As AWS grows, the cost of doing business is reduced and savings are passed back to the customer with lower pricing. What is this optimization called? (Select the best answer.)

- [ ] Expenditure awareness
- [ ] Economies of scale
- [ ] Matching supply and demand
- [ ] EC2 Right Sizing


----

### 8. Frage

True or false? AWS offers some services at no charge, such as, Amazon Virtual Private Cloud, AWS Identity and Access Management, Consolidated Billing, AWS Elastic Beanstalk, automatic scaling, AWS OpsWorks and AWS CloudFormation. However, you might be charged for other AWS services that you use in conjunction with these services.

- [ ] True
- [ ] False 

----

### 9. Frage

What are benefits of using AWS Organizations? (Choose two.)

- [ ] Replaces existing AWS Identity and Access Management (IAM) policies with service control policies (SCPs), which are simpler to manage
- [ ] Provides the ability to create groups of accounts and then attach policies to a group
- [ ] Provides the ability to create an unlimited number of nested organizational units (OUs) to support your desired structure
- [ ] Simplifies automating account creation and management by using APIs
- [ ] Prevents any restrictions from being put on the root user that is associated with the main organization in an account

----

### 10. Frage

True or false? Unlimited services are available with the AWS FreeTier to new AWS customers for 12 months following ther AWS sign-up date. (Select the best answer.)

- [ ] True
- [ ] False 


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
