# Modul 8 - Databases

## Folgende Topics werden in diesem Modul behandelt:

- Amazon Relational Database Service (Amazon RDS)
- Amazon DynamoDB
- Amazon Redshift
- Amazon Aurora

## Ziele dieses Moduls

- Du kannst den DB-Dienst **Amazon Relational Database Service (Amazon RDS)** erklären
- Du kennst die Funktionalität von Amazon RDS
- Du kannst den DB-Dienst **Amazon DynamoDB** erklären und kennst ein Anwendungsbeispiel
- Du kennst die Funktionalität von Amazon DynamoDB
- Du kannst den DB-Dienst **Amazon Redshift** erklären und kennst ein Anwendungsbeispiel
- Du kannst den DB-Dienst **Amazon Aurora** erklären und kennst ein Anwendungsbeispiel

* [Präsentation](./AcademyCloudFoundations_Module_08.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
