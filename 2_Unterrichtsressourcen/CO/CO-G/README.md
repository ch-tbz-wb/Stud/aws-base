# Modul 7 - Networking

## Folgende Topics werden in diesem Modul behandelt:

- Netzwerk und Amazon Virtual Private Cloud (Amazon VPC)
- Sicherung Ihres Netzwerks
- Fehlerbehebung bei Netzwerken auf AWS


## Ziele dieses Moduls

- Du kannst die grundlegende Rolle einer Amazon Virtual Private Cloud (VPC) im AWS Cloud Networking erklären
- Du kennst die Netzwerkkomponenten innerhalb einer VPC und ihren Zweck
- Du kennst die verschiedenen Optionen für die VPC-Konnektivität
- Du kannst das mehrschichtige Netzwerkverteidigungsmodell innerhalb einer VPC erklären
- Du kennst verschiedene Ansätze zur Behebung häufiger VPC-Netzwerkprobleme
- Du kannst eine VPC konfigurieren


* [Präsentation](./ACO-07-Networking.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
