# Cloud Operations - Course Introduction (für Fortgeschrittene)

## Nach Abschluss dieses Moduls kennst Du:

- den Zweck des AWS Academy Cloud Operations-Kurses
- die AWS-Dokumentationswebseiten

## Ziele des Cloud Operations-Kurses

- Verstehen der AWS-Infrastruktur in Bezug auf den Systembetrieb, wie z. B. die globale Infrastruktur, Kerndienste und Kontosicherheit
- Anwendung des AWS Command Line Interfaces (AWS CLI) und weiteren Verwaltungs- und Entwicklungstools
- Verwalten, sichern und skalieren von Recheninstanzen auf AWS
- Verwalten, sichern und skalieren mittels Konfigurationen
- Identifizieren von Container-Services und AWS-Services, die für serverloses Computing verfügbar sind
- Verwalten, sichern und skalieren von Datenbanken auf AWS
- Erstellen virtueller privater Netzwerke mit Amazon Virtual Private Cloud (Amazon VPC)
- Konfigurieren und verwalten von Speicheroptionen mithilfe der von AWS angebotenen Speicherservices
- Überwachung der Infrastruktur mit Services wie Amazon CloudWatch, AWS CloudTrail und AWS Config
- Verwalten des Ressourcenverbrauchs in einem AWS-Konto mithilfe von Tags, Amazon CloudWatch und AWS Trusted Advisor
- Erstellen und konfigurieren von automatisierten und wiederholbaren Umgebungen mit Tools wie Amazon Machine Images (AMIs) und AWS CloudFormation

* [Präsentation](./ACO-00-Course-Welcome-and-Overview.pdf)

<br>

**Dokumentation:**
* [AWS Online Dokumentation](https://docs.aws.amazon.com/)
* [AWS CLI Command Reference](https://docs.aws.amazon.com/cli/latest/index.html)



<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
