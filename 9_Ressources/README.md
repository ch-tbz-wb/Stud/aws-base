[10]: https://git-scm.com/downloads

Ressourcen
====

Für weitere Informationen zu den einzelnen Topics, bitte unten jeweils den entsprechenden **Link** klicken

### Clips
- [Liste](clips/README.md)

---

### Links
- [Liste](links/README.md)

---

### Literatur
- [Liste](docu/README.md)

---

### Diverses 
- [Liste](04-Diverses/README.md)


<br> 

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/armindoerzbachtbz/cloud-native-bootcamp)


---