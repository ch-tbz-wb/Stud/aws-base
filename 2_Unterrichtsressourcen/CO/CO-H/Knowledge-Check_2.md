# Modul 8 - Knowledge Check

### 1. Frage

Where is an Amazon Elastic Block Store (Amazon EBS) volume tied to?

- [ ] Availability Zones
- [ ] Data facility
- [ ] Edge location
- [ ] Regions


----

### 2. Frage

Which of the following need to be defined for an Amazon Data Lifecycle Manager policy? (Select THREE)

- [ ] Resource Type
- [ ] Target tag
- [ ] Region
- [ ] Availability zone
- [ ] Schedule


----

### 3. Frage

Amazon Elastic Block Store (Amazon EBS) snapshots are based on deltas, which means that only changes to previous snapshots are saved to Amazon Simple Storage Service (Amazon S3)

- [ ] True
- [ ] False

----

### 4. Frage

Which storage solution can e network-attached to an Amazon Elastic Compute Cloud (Amazon EC2) instance? 

- [ ] Amazon Elastic Block Store (Amazon EBS)
- [ ] Instance store (ephemeral)
- [ ] Amazon Simple Storage Service Glacier
- [ ] Amazon Simple Storage Service (Amazon S3)


----

### 5. Frage

After versioning is enabled on an Amazon Simple Storage Service (Amazon S3) bucket, it can then be disabled

- [ ] True
- [ ] False

----

### 6. Frage

Which services can be used with Amazon Simple Storage Service (Amazon S3) to receive a notification of a bucket change?

- [ ] AWS Lambda
- [ ] Amazon Simple Notification Service (Amazon SNS)
- [ ] Amazon Simple Queue Service (Amazon SQS)
- [ ] All of the above

----

### 7. Frage

Which solution is best suited as a cache or buffer?

- [ ] Amazon Elastic Block Store (Amazon EBS)
- [ ] Amazon Elastic File System (Amazon EFS)
- [ ] Amazon Simple Storage Service Glacier
- [ ] Amazon Simple Storage Service (Amazon S3)
- [ ] Instance store (ephemeral)

----

### 8. Frage

How much time should it take for archived data to be retrieved by using the bulk retrieval option for Amazon Simple Storage Service Glacier?

- [ ] 1-5 minutes
- [ ] 3-5 hours
- [ ] 5-12 hours
- [ ] 1-2 days

----

### 9. Frage

Amazon Elastic File System (Amazon EFS) can span across multiple Availability Zones in different Regions

- [ ] True
- [ ] False


----

### 10. Frage

Which bucket versioning state results in the addition of a marker when an object is deleted?

- [ ] Unversioned
- [ ] Versioning-enabled
- [ ] Versioning-suspended
- [ ] None of the above


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
