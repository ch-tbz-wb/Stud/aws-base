# Modul 9 - Monitoring and Security

## Folgende Topics werden in diesem Modul behandelt:

- Amazon CloudWatch
- Amazon CloudWatch-Überwachung
- Amazon CloudWatch-Ereignisse
- Amazon CloudWatch-Protokolle
- AWS CloudTrail
- AWS-Konfiguration


## Ziele dieses Moduls

- Du kennst die Vorteile von Amazon CloudWatch
- Du kannst die CloudWatch-Überwachungsfunktionen, einschließlich Metriken und Alarmdetails beschreiben
- Du kannst die CloudWatch-Protokollfunktionen und -vorteile beschreiben
- Du kannst den Zweck und die Funktion von AWS CloudTrail erklären
- Du kannst dieFunktionen und Vorteile von AWS Config erklären
- Du weisst, wie man Amazon CloudWatch zur Überwachung von Anwendungen und Infrastrukturen einsetzt


* [Präsentation](./ACO-09-Monitoring-and-Security.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
