![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# AWS - Cloud Service Provider - AWS Cloud

## Kurzbeschreibung des Moduls 

Dieser Kurs unterstützt Dich dabei, das grundsätzliche Konzept von **AWS** Cloud-Computing kennenzulernen und auch mit Praxisbeispielen anzuwenden. Für diejenigen, welche sich später zertifizieren wollen, bietet dieses Modul einen soliden Einstieg. Wir werden uns mit Cloud-Konzepten befassen und anhand von verschiedenen Use-Cases diverse Hands-on Übungen durchführen, sowie mögliche Prüfungsfragen der «AWS Cloud Practitioner» Zertifizierung behandeln. 

Um später die «AWS Cloud Practitioner»-Zertifizierung zu bestehen, ist es unerlässlich, sich ausreichend Zeit zu nehmen, um das erforderliche Wissen zu erlangen. Dieser Kurs unterstützt Dich dabei, das grundsätzliche Konzept von Cloud-Computing kennenzulernen und auch mit Praxisbeispielen anzuwenden. Er deckt aber nicht die gesamte Vorbereitung auf die Zertifizierung ab.  

## Angaben zum Transfer der erworbenen Kompetenzen 

* Gutes Grundverständnis der wichtigsten AWS Cloud-Konzepte, einschließlich ihrer **Vor- und Nachteile** bzgl. On-Prem Lösungen.
* **Shared Responsibility Model**: Rechte und Pflichten des Providers und der Anwender. 
* Solides Verständnis für die Anwendungsbereiche der wichtigsten AWS-Dienste, wie EC2, S3, VPC und IAM. 
* Erste Hands-on Erfahrungen mit verschiedenen AWS-Services. 
* Überblick und Kontrolle möglicher Kosten bei genutzten Services.
* Die gängigsten AWS-Architektur-Best Practices. 
* Form, Umfang und Anforderungen für eine solide Vorbereitung auf die Einstiegszertifizierung «AWS Cloud Practitioner»

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

* HF-Vorbereitungskurs (Optional)

### Nachfolgende Module

* Azure Cloud Foundation

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Ressourcen

[Ressourcen](./9_Ressources/) Diverse Links


### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
