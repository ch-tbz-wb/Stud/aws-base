# Organisatorisches 

## Autorenschaft

* **Marcello Calisto** - marcello.calisto@tbz.ch
* https://gitlab.com/ser-cal
* https://github.com/ser-cal

---

* **Marcel Bernet** - marcel.bernet@tbz.ch
* https://gitlab.com/mc-b
* https://github.com/mc-b