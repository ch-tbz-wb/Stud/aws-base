# Modul 10 - Automatic Scaling and Monitoring

## Folgende Topics werden in diesem Modul behandelt:

- Elastic Load Balancing
- Amazon CloudWatch
- Amazon EC2 Auto Scaling

## Ziele dieses Moduls

- Du kannst das AWS Well-Architected Framework beschreiben - einschließlich den sechs Säulen



- Du weisst, wie der Datenverkehr mithilfe von Elastic Load Balancing über Amazon Elastic Compute Cloud (Amazon EC2)-Instanzen verteilt wird
- Du weisst, wie mit Amazon CloudWatch AWS-Ressourcen und -Anwendungen in Echtzeit überwacht werden können
- Du kannst erläutern, wie ein **Amazon EC2 Auto Scaling Server** auf Workload-Änderungen reagiert (Instanzen starten und freigeben)
- Du bist in der Lage, Skalierungs- und Lastausgleichsaufgaben durchzuführen, um eine Architektur zu verbessern

* [Präsentation](./AcademyCloudFoundations_Module_10.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
