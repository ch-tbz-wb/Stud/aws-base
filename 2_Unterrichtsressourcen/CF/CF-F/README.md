# Modul 6 - Compute (Section 1 - 3, Lab 3)

## Folgende Topics werden in diesem Modul behandelt:

- Überblick über Compute-Dienste
- Amazon EC2
- Amazon EC2-Kostenoptimierung
- Containerdienste
- Einführung in AWS Lambda
- Einführung in AWS Elastic Beanstalk

## Ziele dieses Moduls

- Du hast einen Überblick über verschiedene AWS-Datenverarbeitungsdienste in der Cloud
- Du kannst erklären, warum Amazon Elastic Compute Cloud (Amazon EC2) verwendet werden sollte
- Du kennst die Funktionalität in der EC2-Konsole
- Du kannst grundlegende Funktionen in Amazon EC2 ausführen, um eine virtuelle Computerumgebung aufzubauen
- Du kennst die Amazon EC2-Kostenoptimierungselemente
- Du weisst, wann AWS Elastic Beanstalk verwendet werden sollte
- Du weisst, wann AWS Lambda verwendet werden sollte
- Du verstehst, wie containerisierte Anwendungen in einem Cluster von verwalteten Servern ausgeführt werden können

* [Präsentation](./AcademyCloudFoundations_Module_06.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
