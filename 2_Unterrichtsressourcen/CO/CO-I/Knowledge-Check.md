# Modul 9 - Knowledge Check

### 1. Frage

Amazon CloudWatch alarms are issued in near real-time in response to changes

- [x] True
- [ ] False


----

### 2. Frage

Amazon CloudWatch Events can be set to automatically trigger based on rate expression

- [x] True
- [ ] False


----

### 3. Frage

What is a name/value pair that is used for metric identification, and can assist with the design structure for a statistics plan?

- [x] Dimension
- [ ] Kubernetes
- [ ] Metric
- [ ] Namespace
- [ ] Period

----

### 4. Frage

Which of the following AWS services can be used with AWS CloudTrail to store event logs?

- [ ] Amazon Elastic Block Store
- [ ] Amazon Simple Storage Service
- [x] Amazon Simple Storage Service (Glacier)
- [ ] Amazon Aurora


----

### 5. Frage

What does AWS CloudTrail allow an IT administrator to determine for a given request made to AWS Config? (Select FOUR.)

- [ ] Duration of the request
- [x] Source IP address of the request
- [x] Time of the request
- [x] Type of request
- [x] Who made the request


----

### 6. Frage

Which feature of Amazon CloudWatch would you use to publish a statistic on service memory usage for an HTTP server running on an Amazon EC2 instance?

- [ ] Common metrics
- [x] Custom metrics
- [ ] Alarms
- [ ] Notifications

----

### 7. Frage

An Amazon CloudWatch event indicates a change in a resource in your AWS environment

- [x] True
- [ ] False

----

### 8. Frage

What are the possible states of an Amazon CloudWatch alarm? (Select THREE.)

- [x] Alarm
- [x] Insufficient Data
- [x] OK
- [ ] Terminated
- [ ] Versioning Enabled

----

### 9. Frage

AWS Config can provide a list of all AWS resources used by an account

- [x] True
- [ ] False


----

### 10. Frage

Which AWS service monitors a metric and sends an alert when the metric changes?

- [ ] Amazon CloudTrail Dashboards
- [ ] Amazon CloudWatch Metrics
- [x] Amazon CloudWatch Alarms
- [ ] Amazon CloudWatch Dashboards
- [ ] Amazon CloudTrail Alerts


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

----
