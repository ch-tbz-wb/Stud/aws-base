# Modul 4 - Hands-on

## Ergänzungsauftrag:

Bitte spiele diesen Auftrag erst durch, wenn Du den **Lab-Auftrag** auf AWS Academy **durchgeführt und abgeschlossen** hast.

## Ausgangslage

**user-1** aus der Storage-Gruppe hat ein firmeninternes Angebot von der Abteilung **EC2-Administration** erhalten. Der Stellenwechsel ist bereits erfolgt. 
Gerade eben hat **user-1** von seinem Vorgesetzten den Auftrag erhalten, die EC2-Instanz **Bastion Host** zu terminieren. Bei der Ausführung des Auftrages, stellt **user-1** fest, dass ihm dazu (noch) die Berechtigungen fehlen. 
Sein Vorgesetzter meldet sich deshalb bei Dir und möchte, dass Du **user-1** schnellstmöglich dafür berechtigst.

## To do
- Überlege Dir zuerst, welche Schritte notwendig sind
- Führe die entsprechenden Anpassungen aus
- Überprüfe, ob Du die richtigen Berechtigungen gesetzt hast
- Logge Dich zuerst mit **user-1** aus und dann wieder ein, damit die neuen Berechtigungen greifen 
- Führe den geforderten Task mit **user-1** aus. 
- Dokumentiere die Schritte


* ![Ausgangslage](./01_Organigramm_800.png)

---

### Credentials

- **Username:** user-1
- **Passwort:** Lab-Password1

----

- **Username:** user-2
- **Passwort:** Lab-Password2

----

- **Username:** user-3
- **Passwort:** Lab-Password3


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
