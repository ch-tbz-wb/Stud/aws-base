# Modul 6 - Knowledge Check

### 1. Frage

Why is AWS more economical than traditional data centers for applications with varying compute workloads? (Select the best answer.)

- [ ] Amazon EC2 costs are billed on a monthly basis
- [ ] Customers retain full administrative access to their Amazon EC2 instances
- [ ] Customers can permanently run enough instances to handle peak workloads
- [x] Amazon EC2 instances can be launched on-demand when needed

----

### 2. Frage

If your project requires monthly reports that iterate through very large amounts of data, which Amazon Elastic Compute Cloud (Amazon EC2) purchasing option should you consider? (Select the best answer.)

- [ ] Spot Instances
- [x] Scheduled Reserved Instances
- [ ] Dedicated Hosts
- [ ] On-Demand Instances

----

### 3. Frage

What is included in an Amazon Machine Image (AMI)? (Select the best answer.)

- [ ] A template for the root volume for the instance
- [ ] Launch permissions that control which AWS accounts can use the AMI to launch instances
- [ ] A block device mapping that specifies the volumes to attach to the instance when it's launched
- [x] All of the above

----

### 4. Frage

Which Amazon Elastic Compute Cloud (Amazon EC2) feature ensures your instances will not share a physical host with instances from any other AWS customer? (Select the best answer.)

- [ ] Amazon VPC
- [ ] Placement groups
- [x] Dedicated Instances
- [ ] Reserved Instances

----

### 5. Frage

Which of the following services is a serverless compute service in AWS? (Select the best answer.)

- [ ] AWS Config
- [x] AWS Lambda
- [ ] AWS OpsWork
- [ ] Amazon EC2


----

### 6. Frage

What is the service provided by AWS that enables developers to easily deploy and manage applications in the cloud? (Select the best answer)

- [ ] Amazon Elastic Container Service
- [x] AWS Elastic Beanstalk
- [ ] AWS OpsWork
- [ ] AWS CloudFormation

_`(Elastic Beanstalk ist eine AWS Compute Service option. Dieser "Platform as a Service - PAAS-Dienst", unterstützt Anwender dabei, Web-Applikationen schnell zu deployen, zu skalieren und zu administrieren)`_


----

### 7. Frage

Your web application needs four instances to support steady traffic all of the time. On the last day of the month, the traffic triples. What is the most cost-effective way to handle this pattern? (Select the best answer)

- [ ] Run 12 Reserved Instances all of the time
- [ ] Run four On-Demand Instances constantly, then add eigth more On-Demand Instances on the last day of the month
- [x] Run four Reserved Instances constantly, then add eight On-Demand Instances on the last day of each month
- [ ] Run four On-Demand Instances constantly, then add eight Reserved Instances on the last day of each month

----

### 8. Frage

True or False? Containers contain an entire operating system

- [ ] True
- [x] False


----

### 9. Frage

Which Amazon EC2 option is best for long-term workloads with predictable usage patterns? (Select the best answer.)

- [ ] Spot Instances
- [ ] On-Demand Instances
- [x] Reserved Instances


----

### 10. Frage

Which of the following must be specified when launching a new Amazon Elastic Compute Cloud (Amazon EC2) Windows instance? (Choose two)

- [ ] The Amazon EC2 instance ID
- [ ] Password for the administrator account
- [x] Amazon EC2 instance type
- [x] Amazon Machine Image (AMI)


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
