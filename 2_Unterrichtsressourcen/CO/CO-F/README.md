# Modul 6 - Computing Database Services

## Folgende Topics werden in diesem Modul behandelt:

- AWS-Datenbankdienste
- Amazon Relational Database Service (Amazon RDS)
- Amazonas-Aurora
- Amazon DynamoDB
- AWS Database Migration Service (AWS DMS)

## Ziele dieses Moduls

- Du kennst die unterschiedlichen Arten von verwalteten Datenbankdiensten, die von Amazon Web Services (AWS) angeboten werden
- Du kennst die Faktoren, die bei der Auswahl einer Datenbank (Engine und Workloads) zu berücksichtigen sind
- Du kannst den Zweck und die Funktion von Amazon Relational Database Service (Amazon RDS), Amazon Aurora und Amazon DynamoDB erklären und kennst die jeweiligen Vorteile
- Du kennst die wichtigsten Funktionen und Vorteile von Amazon Relational Database Service (Amazon RDS), Amazon Aurora und Amazon DynamoDB
- Du kannst die Vorteile des AWS Database Migration Service (AWS DMS) und die Funktionen des AWS Schema Conversion Tools (AWS SCT) beschreiben


* [Präsentation](./ACO-06-Computing-Database-Services.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
