# Modul 6 - Knowledge Check

### 1. Frage

Which AWS database service would increase availability and performance by enabling multiple replicas?

- [x] Amazon Aurora
- [ ] Amazon DynamoDB
- [ ] Amazon Relational Database Service (Amazon RDS)
- [ ] Amazon Redshift

_`(Amazon Aurora kann innerhalb einer Region pro DB-Cluster bis zu 15 Replicas haben)`_


----

### 2. Frage

Which feature of Amazon DynamoDB builds a global footprint by replicating tables automatically across selected Regions?

- [ ] Integrated monitoring
- [x] Global Table
- [ ] Encryption at rest
- [ ] Amazon DynamoDB Accelerator (DAX)


----

### 3. Frage

A cloud-based bank created a new credit card line. The bank must expand their database to capture new customer profiles and link existing customer profiles to the new card line. They must also maintain security and compliance with the Payment Card Industry Data Security Standard (PCI-DSS). Their customers can use a mobile app or a website for account transactions and profiles, and they can also view visualization based on their financial data. Which AWS service should be prioritized for the bank's database if they want to graph connected data about their customer's financial behaviors?

- [ ] Amazon Aurora
- [ ] Amazon DynamoDB
- [ ] Amazon ElastiCache
- [x] Amazon Neptune
- [ ] Amazon Redshift

_`(Stichwort: to graph connected data)`_

----

### 4. Frage

A customer decided to migrate from a MySQL database that runs in their data center to Amazon Aurora.
Which AWS service or AWS service feature should be used to automatically convert data throughout the migration?

- [ ] AWS Data Sync
- [x] AWS Database Migration Service (AWS DMS)
- [ ] Amazon Aurora storage engine
- [ ] All of these


----

### 5. Frage

Amazon Relational Database Service (Amazon RDS) handles time-consuming administrative tasks, such as patching and backups

- [x] True
- [ ] False

----

### 6. Frage

NoSQL database schemas are dynamic

- [x] True
- [ ] False

----

### 7. Frage

You are the SysOps administrator for a small global shipping organization. You must reduce costs and administrative overhead in your company's PostgreSQL environments. The most important requirements for this project are maintaining performance, scalability, and compatibility.

Which AWS service would meet the project requirements?

- [ ] Amazon DynamoDB
- [ ] Amazon ElastiCache
- [x] Amazon Relational Database Service (Amazon RDS)
- [ ] Amazon Neptune

_`(Stichwort: PostgreSQL - wird von RDS gemanaged)`_

----

### 8. Frage

What is a NoSQL database option?

- [ ] Amazon Aurora
- [x] Amazon DynamoDB
- [ ] Amazon Relational Database Service (Amazon RDS)
- [ ] MariaDB
- [ ] PostgreSQL


----

### 9. Frage

Wich AWS service should a customer consider if they need a relational database that is compatible with MySQL and PostgreSQL and that is also built for the cloud?

- [x] Amazon Aurora
- [ ] Amazon DynamoDB
- [ ] Amazon Relational Database Service (Amazon RDS)
- [ ] All of the above

_`(Stichwort: Kompatibel mit MySQL UND PostgreSQL)`_

----

### 10. Frage

What is the maximum number of read replicas that an Amazon Aurora database cluster can have?

- [ ] 25
- [x] 15
- [ ] 5
- [ ] 10
- [ ] 20



<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
