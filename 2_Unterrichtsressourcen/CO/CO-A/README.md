# Modul 1 - Understanding Systems Operations on AWS

## Folgende Topics werden in diesem Modul behandelt:

- Systembetrieb in der Cloud
- Einführung in die Kerndienste (Core Services)
- AWS Identitäts- und Zugriffsverwaltung (IAM)
- AWS-Befehlszeilenschnittstelle (AWS CLI)

## Ziele dieses Moduls

- Du kannst Systemvorgänge in der Cloud beschreiben, die im Zusammenhang stehen mit automatisierten und wiederholbaren "Deployments"
- Du kennst Kriterien, die bei der Wahl von Amazon Web Services (AWS)-Regionen und Edge-Standorte berücksichtigt werden sollten
- Du kannst die Kerndienste im Zusammenhang mit Systemoperationen, Diensten für Netzwerk, Datenverarbeitung und Zugriff beschreiben
- Du kannst erläutern, wie AWS Identity and Access Management (IAM) Sicherheit für AWS-Kontoressourcen bietet
- Du kannst die Funktionen der AWS-Befehlszeilenschnittstelle (AWS CLI) beschreiben


* [Präsentation](./ACO-01-Understanding-Systems-Operations-on-AWS.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_



<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
