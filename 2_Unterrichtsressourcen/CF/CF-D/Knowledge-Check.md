# Modul 4 - Knowledge Check

### 1. Frage

In the shared responsibility model, AWS is responsible for providing what? (Select the best answer.)

- [x] Security of the cloud
- [ ] Security to the cloud
- [ ] Security for the cloud
- [ ] Security in the cloud

----

### 2. Frage

In the shared responsibility model, which of the following are examples of "security in the cloud"? (Choose two.)

- [ ] Compliance with compute security standards and regulations
- [ ] Physical security of the facilities in which the services operate
- [x] Security group configurations
- [x] Encryption of data at rest and data in transit
- [ ] Protecting the global infrastructure


----

### 3. Frage

Which of the following is the responsibility of AWS under the AWS shared responsibility model? (Select the best answer.)

- [ ] Configuring third-party applications
- [x] Maintaining physical hardware
- [ ] Security application access and data
- [ ] Managing custom Amazon Machine Images (AMIs)

----

### 4. Frage

When creating an AWS Identity and Access Management (IAM) policy, what are the two types of access that can be granted to a user? (Choose two.)

- [ ] Institutional access
- [ ] Authorized access
- [x] Programmatic access
- [x] AWS Management Console access
- [ ] Administrative root access

----

### 5. Frage

True or false? AWS Organizations enables you to consolidate multiple AWS accounts so that you centrally manage them.

- [x] True
- [ ] False 


----

### 6. Frage

Which of the following are best practices to secure your account using AWS Identity and Access Management (IAM)? (Choose two.)

- [ ] Provide users with default administrative privileges
- [ ] Leave unused and unnecessary users and credentials in place
- [x] Manage access to AWS resources
- [ ] Avoid using IAM groups to grant the same access permissions to multiple users
- [x] Define fine-grained access rights

----

### 7. Frage

Which of the following should be done by the AWS account root user? (Select the best answer)

- [ ] Secure access for applications
- [ ] Integrate with ohter AWS services
- [ ] Change granular permissions
- [x] Change the AWS support plan

_`(Von den Antworten oben kann nur der "AWS support Plan" vom Root-User geändert werden. Die anderen Tasks werden mit IAM erledigt)`_

----

### 8. Frage

After initial login, what does AWS recommend as the best practice for the AWS account root user? (Select the best answer)

- [ ] Delete the AWS account root user
- [ ] Revoke all permissions on the AWS account root user
- [ ] Restrict permissions on the AWS account root user
- [x] Delete the access keys of the AWS account root user

_`(AWS empfiehlt, die "Access Keys" des AWS account root users nach dem ersten Login zu löschen)`_

----

### 9. Frage

How would a system administrator add an additional layer of login security to a user's AWS Management Console? (Select the best answer.)

- [ ] Use Amazon Cloud Directory
- [ ] Audit AWS Identity and Access Management (IAM) roles
- [x] Enable multi-factor authentication
- [ ] Enable AWS CloudTrail

----

### 10. Frage

True or false? AWS Key Management Service (AWS KMS) enables you to assess, audit, and evaluate the configurations of your AWS resources.

- [ ] True
- [x] False

_`(AWS KMS ist ein Service, mit dem Encryption-Keys erstellt und verwaltet werden. So kann die Nutzung dieser Keys gleich über eine Vielzahl von AWS Services und Applikationen gesichert und orchestriert werden)`_


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
