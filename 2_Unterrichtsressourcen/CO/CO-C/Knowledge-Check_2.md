# Modul 3 - Knowledge Check

### 1. Frage

What provides the required information that is needed to launch an Amazon Elastic Compute Cloud (Amazon EC2) instance?

- [ ] Amazon Machine Image (AMI)
- [ ] Launch Permissions
- [ ] Instance Profile
- [ ] None of the above 


----

### 2. Frage

Network performance can be impacted by the instance type that is selected for use

- [ ] True
- [ ] False


----

### 3. Frage

A SysOps Engineer who works at a local university decided to launch Amazon Elastic Compute Cloud (Amazon EC2) instances to reduce their hardware footprint. They are new to AWS and only need to launch in one Region until ther business grows

What is the first step when you launch an EC2 instance?

- [ ] Determine the Amazon Machine Image (AMI) that will be used
- [ ] Configure launch access and control permissions
- [ ] Provide configuration details
- [ ] Select instance type and size

----

### 4. Frage

A new billing hour begins when an instance is restarted

- [ ] True
- [ ] False


----

### 5. Frage

Which Amazon Elastic Compute Cloud (Amazon EC2) lifecycle state retains the public IP address of the instance?

- [ ] Running
- [ ] Stopped
- [ ] Terminated
- [ ] All of these

----

### 6. Frage

What enables an AWS Identity and Access Management (IAM) role information to be attached to an Amazon Elastic Compute Cloud (Amazon EC2) instance?

- [ ] Amazon Machine Image (AMI)
- [ ] Amazon Elastic Container Service (Amazon ECS)
- [ ] Instance profile
- [ ] All of these


----

### 7. Frage

Data that is stored in an instance store is preserved even when the instance is stopped.

- [ ] True
- [ ] False


----

### 8. Frage

Amazon EBS-optimized instances can provide better performance than other EBS storage options.

- [ ] True
- [ ] False


----

### 9. Frage

Which block storage option is best suited for I/O-intensive database and application workloads with extremly low latency?

- [ ] All of these
- [ ] Magnetic Storage
- [ ] General Purpose (SSD) Storage
- [ ] Provisioned IOPS (SSD) Storage


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
