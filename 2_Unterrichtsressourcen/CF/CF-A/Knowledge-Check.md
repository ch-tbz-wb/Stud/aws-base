# Modul 1 - Knowledge Check

### 1. Frage

What are the advantages of cloud computing over computing on-premises? (Select the best answer.)

- [ ] Avoid large capital purchases
- [ ] use on-demand capacity
- [ ] Go global in minutes
- [ ] Increase speed and agility
- [x] All of the above

----

### 2. Frage

What is the pricing model that enables AWS customers to pay for resources on an as-needed basis? (Select the best answer.)

- [ ] Pay as you decommission
- [x] Pay as you go
- [ ] Pay as you buy
- [ ] Pay as you reserve

----

### 3. Frage

Which of these is NOT a cloud computing model?

- [ ] Platform as a service
- [ ] Infrastructure as a service
- [x] System administration as a service
- [ ] Software as a service

----

### 4. Frage

True or False? AWS owns and maintains the network-connected hardware required for application services, while you provision and use what you need.

- [x] True
- [ ] False 

----

### 5. Frage

Which of these is NOT a benefit of cloud computing over on-premises computing? (Select the best answer.)

- [ ] Increase speed and agility
- [x] Pay for racking, stacking, and powering servers
- [ ] Eliminate guessing on your infrastructure capacity needs
- [ ] Trade capital expense for variable expense
- [ ] Benefit from massive economic of scale

----

### 6. Frage

Which of the following are NOT benefits of AWS Cloud computing? (Choose two.)

- [x] Multiple procurement cycles
- [ ] High availability
- [x] High latency
- [ ] Temporary and disposable resources
- [ ] Fault-tolerant databases

----

### 7. Frage

Which of the following is a compute service? (Select the best answer)

- [ ] Amazon VPC
- [ ] Amazon S3
- [x] Amazon EC2
- [ ] Amazon CloudFront
- [ ] Amazon Redshift


----

### 8. Frage

True or False? Cloud computing provides a simple way to access servers, storage, databases, and a broad set of application services over the internet. You own the network-connected hardware required for these services and Amazon Web Services provisions what you need

- [ ] True
- [x] False 

----

### 9. Frage

Economies of scale result form ________. (Select the best answer.)

- [ ] having many different cloud providers
- [x] having hundreds of thousands of customers aggregated in the cloud
- [ ] having hundreds of cloud services available over the internet
- [ ] havin to invest heavily in data centers and servers

----

### 10. Frage

Which of these are ways to access AWS core services? (Choose three.)

- [ ] Technical support calls
- [ ] AWS Marketplase
- [x] AWS Management Console
- [x] AWS Command Line Interface (AWS CLI)
- [x] Software Development Kits (SDKs)


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
