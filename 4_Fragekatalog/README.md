# Zertifizierungsfragen

[[_TOC_]]

## Cloud Practitioner
Bei der Zertifizierung zum **Cloud Practitioner (CLF-C01)** gilt es, insgesamt **65 Fragen** in **90 Minuten** zu beantworten. Wichtig in diesem Zusammenhang ist auch, dass **15** dieser Fragen nicht bewertet werden. **AWS** baut diese ein, um sie später auszuwerten und allenfalls irgendwann in den Fragenpool der Zertifizierung aufzunehmen. Also: Nicht verunsichern lassen, wenn während der Zertifizierung plötzlich Fragen auftauchen, die man **nur mit raten** beantworten kann :-)

Die Fragen setzen sich aus **zwei** Arten zusammen:

- **Multiple Choice:** Beinhaltet eine richtige und drei falsche Antworten (Distraktoren)
- **Mehrfache Antwort:** Beinhaltet zwei oder mehr korrekte Antworten aus fünf oder mehr Antwortoptionen.

Unten nun eine Auswahl von 5 typischen Fragen. Bei den Antworten jeweils mit **(R)** ichtig oder **(F)** alsch gekennzeichnet:

### Frage 1

*Under the shared responsibility model, which of the following activities are customers’ responsibilities? (Choose TWO)*

<details><summary>Antworten</summary>
<p>

- Upgrading the underlying infrastructure for compute resources **(F)**
- Security in the Cloud **(R)**
- Patching operating system for Relational Database Service (RDS) **(F)**
- Security of the Cloud **(F)**
- Configuring the security groups **(R)**

</p>
</details>

---

### Frage 2

*What does AWS recommend when designing a high available architecture on the AWS Cloud? (Choose TWO)*

<details><summary>Antworten</summary>
<p>

- Avoid reserving compute capacity upfront **(F)**
- Use monolithic architecture **(F)**
- Anticipate failure **(R)**
- Always deploy applications in multiple regions **(F)**
- Make frequent, small, reversible changes to your code **(R)**

</p>
</details>

---

### Frage 3

*What does principle of Least Privilege mean in the context of AWS security?*

<details><summary>Antworten</summary>
<p>

- Avoid reserving compute capacity upfront **(F)**
- Granting the least amount of resources **(F)**
- Granting only the permissions required to perform a task **(R)**
- Apply least number of security groups to EC2 instances **(F)**
- Make least number of changes to the default security settings in AWS account **(F)**

</p>
</details>

---

### Frage 4

*What would be the advantages of moving infrastructure from an on-premises data center to AWS Cloud?*

<details><summary>Antworten</summary>
<p>

- It eliminates the data security risks altogether **(F)**
- It allows to trade variable cost with the operating cost **(F)**
- It eliminates the need to update operating systems on the servers **(F)**
- It eliminates the need to guess infrastructure capacity **(R)**

</p>
</details>

---

### Frage 5

*How should an application’s components be coupled in a microservices architecture?*

<details><summary>Antworten</summary>
<p>

- Tightly coupled **(F)**
- Frequently coupled **(F)**
- Loosely coupled **(R)**
- Intermittently coupled **(F)**

</p>
</details>

---
