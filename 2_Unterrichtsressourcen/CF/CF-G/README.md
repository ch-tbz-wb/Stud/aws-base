# Modul 7 - Storage

## Folgende Topics werden in diesem Modul behandelt:

- Amazon Elastic Block Store (Amazon EBS)
- Amazon Simple Storage Service (Amazon S3)
- Amazon Elastic File System (Amazon EFS)
- Amazon Simple Storage Service Glacier

## Ziele dieses Moduls

- Du kannst zwischen verschiedenen Speicherarten unterscheiden
- Du kannst den Dienst Amazon S3 erklären
- Du kennst die Funktionalität von Amazon S3
- Du kannst den Dienst Amazon EBS erklären
- Du kannst die Funktionalität von Amazon EBS erklären
- Du kannst mit Amazon EBS aus eine Amazon EC2-Speicherlösung zu erstellen
- Du kannst den Dienst Amazon EFS erklären
- Du kennst die Funktionalität von Amazon EFS
- Du kannst den Dienst Amazon S3 Glacier erklären
- Du kennst die Funktionalität von Amazon S3 Glacier
- Du kennst die Unterschiede zwischen Amazon EBS, Amazon S3, Amazon EFS und Amazon S3 Glacier

* [Präsentation](./AcademyCloudFoundations_Module_07.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
