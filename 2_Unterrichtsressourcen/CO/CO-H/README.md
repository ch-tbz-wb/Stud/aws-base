# Modul 8 - Storage and Archiving

## Folgende Topics werden in diesem Modul behandelt:

- Überblick über Cloud-Speicher
- Amazon EBS
- Instanzspeicher
- Amazon Elastic File System (Amazon EFS)
- Amazon S3
- Amazon S3-Gletscher
- AWS-Storage-Gateway
- AWS-Datenübertragungs- und Migrationsservices


## Ziele dieses Moduls

- Du kennst die unterschiedlichen AWS-Datenspeicherungsoptionen und kannst die deren Zweck und Vorteile begründen
- Du kannst Amazon EBS-Snapshots erstellen und verwalten
- Du kannst Amazon S3-Objekte speichern, abrufen und archivieren
- Du kennst die AWS-Datenmigrationsdienste


* [Präsentation](./ACO-08-Storage-and-Archiving.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
