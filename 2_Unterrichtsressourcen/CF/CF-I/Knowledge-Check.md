# Modul 9 - Knowledge Check

### 1. Frage

Which of the following is not one of the four areas of the performance efficiency pillar of the AWS Well-Architected Framework? (Select the best answer.)

- [ ] Tradeoffs
- [ ] Selection
- [x] Traceability
- [ ] Monitoring


----

### 2. Frage

Which of the following is a principle when designing cloud-based systems? (Select the best answer.)

- [ ] Build tightly-coupled components
- [ ] Make infrequent, large batch changes
- [x] Assume everything will fail
- [ ] Use as many services as possible


----

### 3. Frage

Which of the following are pillars of the AWS Well-Architected Framework? (Choose three.)

- [x] Security
- [ ] Persistence
- [x] Operational Excellence
- [x] Cost Optimization


----

### 4. Frage

Which design principles are recommended when considering performance efficiency? (Choose two.)

- [ ] Match supply with demand
- [ ] Enable traceability
- [ ] Analyze and attribute expenditure
- [x] Democratize advanced technologies
- [x] Use serverless architectures


----

### 5. Frage

AWS Trusted Advisor provides insight regarding which five categories of an AWS account? (Select the best answer.)

- [ ] Security, access control, high availability, performance, service limits
- [ ] Performance, cost optimization, access control, connectivity, security
- [x] Performance, cost optimization, security, fault tolerance, service limits
- [ ] Security, fault tolerance, high availability, connectivity, service limits


----

### 6. Frage

What is the focus of the sustainability pillar of the Well-Architected Framework? (Select the best answer.)

- [ ] Designing workloads that recover quickly from failures
- [x] Minimizing the environmental impacts of running cloud workloads
- [ ] Avoiding unnecessary costs in cloud workloads
- [ ] Automating updates to cloud workloads


----

### 7. Frage

After you move to the AWS Cloud, you want to ensure that the right security settings are put in place. Which online tool can assist in security compliance?  (Select the best answer.)

- [ ] Amazon Kinesis
- [ ] AWS Support
- [x] AWS Trusted Advisor
- [ ] Amazon CloudWatch

----

### 8. Frage

Which of the following is a measure of your system's ability to provide functionality when desired by the user? (Select the best answer.)

- [ ] Availability
- [ ] Fault Tolerance
- [x] Reliability
- [ ] Performance efficiency


----

### 9. Frage

What is defined as the ability for a system to remain operational even if some of the components of that system fail? (Select the best answer.)

- [ ] High durability
- [x] Fault tolerance
- [ ] High availability
- [ ] High durability


----

### 10. Frage

Which of the following best describes a system that can withstand some measures of degradation, experiences minimal downtime, and requires minimal human intervention? (Select the best answer.)

- [ ] Scalable
- [ ] Fault-tolerant
- [ ] Elastic
- [x] Highly available


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
