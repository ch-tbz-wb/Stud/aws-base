# Modul 3 - AWS Global Infrastructure Overview (Selbststudium)

## Folgende Topics werden in diesem Modul behandelt:

- Globale AWS-Infrastruktur
- Überblick über AWS-Dienste und Dienstkategorien

## Ziele dieses Moduls

- Du kennst die Begriffe AWS-Regions, Availability Zones und Edge-Standorte
- Du kennst die Begriffe AWS-Services und Servicekategorien

* [Präsentation](./AcademyCloudFoundations_Module_03.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
