# Modul 10 - Knowledge Check

### 1. Frage

Which of the following AWS tools help your application scale up or down based on demand? (Choose two.)

- [ ] Availability Zones
- [ ] Amazon EC2 Auto Scaling
- [ ] AWS CloudFormation
- [ ] Elastic Load Balancer
- [ ] AWS Config

----

### 2. Frage

Which service would you use to send alerts based on Amazon CloudWatch alarms? (Select the best answer.)

- [ ] Amazon Simple Notification Service (Amazon SNS)
- [ ] AWS CloudTrail
- [ ] AWS Trusted Advisor
- [ ] Amazon Route 53


----

### 3. Frage

Which of the following are characteristics of Amazon EC2 Auto Scaling? (Choose three.)

- [ ] Only supports dynamic scaling
- [ ] Responds to changing conditions by adding or terminating instances
- [ ] Delivers push notifications
- [ ] Launches instances from a specified Amazon Machine Image (AMI)
- [ ] Enforces a minimum number of running Amazon EC2 instances


----

### 4. Frage

Which of the following must be configured on an Elastic Load balancing load balancer to expect incoming traffic? (Select the best answer.)

- [ ] A port
- [ ] A network interface
- [ ] A listener
- [ ] An instance

----

### 5. Frage

Which of the following elements are used to create an Amazon EC2 Auto Scaling launch configuration? (Choose three.)

- [ ] Amazon Machine Image (AMI)
- [ ] Load balancer 
- [ ] Instance type
- [ ] Virtual private cloud (VPC) and subnets
- [ ] Amazon Elastic Block Store (Amazon EBS) volumes

----

### 6. Frage

Which of the following services can help you collect important metrics from Amazon Relational Database Service (Amazon RDS) and Amazon Elastic Compute Cloud (Amazon EC2) instances? (Select the best answer.)

- [ ] Amazon CloudFront
- [ ] Amazon CloudSearch
- [ ] Amazon CloudWatch
- [ ] AWS CloudTrail
- [ ] Amazon EC2 Auto Scaling


----

### 7. Frage

Which of the following elements are used to create an Amazon EC2 Auto Scaling launch configuration? (Choose three.)

- [ ] Minimum size
- [ ] Health checks
- [ ] Desired capacity
- [ ] Maximum size

----

### 8. Frage

There is an audit at your company and they need to have a log of all access to AWS resources in the account. Which of the following services can assist in providing these details? (Select the best answer.)

- [ ] Amazon CloudWatch
- [ ] AWS CloudTrail
- [ ] Amazon Elastic Compute Cloud (Amazon EC2)
- [ ] Amazon Simple Notification Service (Amazon SNS)


----

### 9. Frage

In Elastic Load Balancing, when the load balancer detects an unhealthy target, which of the following are true? (Choose three.)

- [ ] Stops routing traffic to that target
- [ ] Triggers an alarm
- [ ] Resumes routing traffic when it detects that the target is healthy again
- [ ] Resumes routing traffic when manually restarted
- [ ] Routes traffic to a healthy target


----

### 10. Frage

What are the three types of load balancers that Elastic Laod Balancing offers?

- [ ] Internet Load Balancer
- [ ] Application Load Balancer
- [ ] Network Load Balancer
- [ ] Compute Load Balancer
- [ ] Classic Load Balancer
- [ ] Auto Scaling Load Balancer


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
