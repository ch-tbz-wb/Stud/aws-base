# Modul 2 - Knowledge Check

### 1. Frage

What type of AWS Systems Manager document can take a snapshot of an Amazon Elastic Compute Cloud (Amazon EC2) instance?

- [ ] Command document
- [ ] Run document
- [ ] Script document
- [ ] Automation document

----

### 2. Frage

You have Bash scripts that you routinely run on your Amazon Elastic Compute Cloud (Amazon EC2) instance that run Linux. 
Which AWS service or AWS service feature provides the simplest way to automate the running of these scripts?

- [ ] Distributor
- [ ] AWS OpsWorks Stacks
- [ ] AWS CodePipeline
- [ ] AWS Systems Manager 


----

### 3. Frage

AWS CloudFormation templates can be written in either JavaScript Object Notation (JSON) or YAML Ain't Markup Language (YAML) files

- [ ] True
- [ ] False


----

### 4. Frage

Which of the follwing are AWS OpsWorks offerings? (Select THREE.)

- [ ] AWS OpsWorks for Chef Automate
- [ ] AWS Tools for PowerShell
- [ ] AWS OpsWorks for Puppet Enterprise
- [ ] AWS OpsWorks Stacks


----

### 5. Frage

The URL for a website hosted in Amazon Simple Storage Service (Amazon S3) can include either a period (.) or a dash (-) to separate the website from the Region name.

- [ ] True
- [ ] False 


----

### 6. Frage

AWS provides a single software development kit (SDK) that covers all programming languages

- [ ] True
- [ ] False 


----

### 7. Frage

Which AWS Systems Manager feature displays operational data for AWS resources?

- [ ] AWS System Manager Agent
- [ ] AWS Systems Manager Insights Dashboard
- [ ] AWS AppConfig
- [ ] None of the above


----

### 8. Frage

Which AWS Service can store configuration data?

- [ ] Systems Manager Parameter Store
- [ ] AWS Secrets Manager
- [ ] Amazon Kinesis
- [ ] None of the above

----

### 9. Frage

You can create set time periods for running administrative tasks with AWS Systems Manager.

- [ ] True
- [ ] False 


----

### 10. Frage

Which AWS Systems Manager feature enables access to an Amazon Elastic Compute Cloud (Amazon EC2) instance without opening inbound ports or bastion hosts?

- [ ] OpsCenter
- [ ] Explorer
- [ ] Session Manager
- [ ] None of the above


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
