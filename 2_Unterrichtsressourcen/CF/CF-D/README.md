# Modul 4 - AWS Cloud Security

## Folgende Topics werden in diesem Modul behandelt:

- AWS shared responsibility model
- AWS Identity and Access Management (IAM) - Identitäts- und Zugriffsverwaltung
- Sichern eines neuen AWS-Kontos
- Sicherung von Konten
- Sichern von Daten auf AWS
- Arbeiten, um die Einhaltung sicherzustellen

## Ziele dieses Moduls

- Du weisst, wie die Verantwortung zwischen Kunden und AWS aufgeteilt wird
- Du weisst, wie man IAM-Benutzer, -Gruppen und -Rollen erkennt
- Du kannst die verschiedene Arten von Sicherheitsanmeldeinformationen in IAM beschreiben
- Du kennst die Vorgehensweise, wie man neue AWS-Kontos sichert
- Du kannst IAM-Benutzer und -Gruppen erkunden
- Du weisst, wie man AWS-Daten sichert
- Du kennst das AWS-Compliance-Program

* [Präsentation](./AcademyCloudFoundations_Module_04.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_
* [Hands-on Ergänzungsauftrag](./ergaenzungsauftrag.md) - _(Mitarbeiter **user-1** hat die Abteilung gewechselt)_




<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
