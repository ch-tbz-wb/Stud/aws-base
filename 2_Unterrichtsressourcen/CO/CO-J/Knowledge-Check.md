# Modul 10 - Knowledge Check

### 1. Frage

Inherited tags count towards the total tag limit for a resource

- [x] True
- [ ] False


----

### 2. Frage

Which AWS service or AWS service feature illustrates the most used resources and the proportional consts of their use?

- [x] Month-to-Date Spend by Service
- [ ] AWS Cost Explorer
- [ ] AWS Budgets
- [ ] Spend Summary

_`(Month-to-Date Spend by Service zeigt grafisch auf, welche Ressourcen am meisten genutzt wurden und welche proportionalen Kosten sie verursachen)`_

----

### 3. Frage

AWS Trusted Advisor can identify underutilized load balancers and volumes

- [x] True
- [ ] False


----

### 4. Frage

Which AWS service can be used to define and enforce required tags?

- [ ] AWS Indentity and Access Management (IAM)
- [x] AWS Config
- [ ] AWS Cost Management 
- [ ] AWS Trusted Advisor


----

### 5. Frage

What is the main difference between the goals of AWS Cost Explorer and of cost and usage reports?

- [ ] Cost and usage reports display visualization of high-level historical and current account costs, but Cost Explorer generates granular usage reports in comma-separated values (CSV) format
- [ ] Cost and usage reports can alert you to malicious intrusions, but Cost Explorer displays visualizations of high-level historicaland current account costs
- [ ] Cost Explorer enables you to set alerts that are triggered by billing events, but cost and usage reports help you visualize system events
- [x] Cost Explorer displays visualization of high-level historical and current account costs, but cost and usage reports generate granular usage reports in comma-separated values (CSV) format

----

### 6. Frage

Which service should be used to find instances that run idle and are increasing costs?

- [x] Amazon CloudWatch
- [ ] AWS Cost Explorer
- [ ] AWS Cost and Usage Report
- [ ] AWS Cost Management

----

### 7. Frage

Which feature of AWS Billing and Cost Management enables you to view costs across linked accounts, and to monitor spending on a daily and monthly basis?

- [x] AWS Budgets
- [ ] Cost Explorer
- [ ] Billing and Cost Management dashboardd
- [ ] Spend Summary

----

### 8. Frage

Using a stopinator script is a best practice for reducing cost

- [x] True
- [ ] False


----

### 9. Frage

AWS Config will only evaluate a rule against resources when the resources are running

- [ ] True
- [x] False


-----

### 10. Frage

You can enforce the use of tags with an AWS Identity and Access Management (IAM) policy

- [x] True
- [ ] False



<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
