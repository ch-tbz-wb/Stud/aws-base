# Modul 7 - Knowledge Check

### 1. Frage

What describes the connection between two or more virtual private clouds (VPCs)?

- [ ] Corporate network connection
- [ ] Data network connection
- [ ] Peering connection
- [ ] Subnet routing option


----

### 2. Frage

How many IP addresses does Amazon VPC reserve in a subnet?

- [ ] 2
- [ ] 6
- [ ] 3
- [ ] 5


----

### 3. Frage

When you create a subnet, which route table (if any) is associated with it by default?

- [ ] Custom route table
- [ ] Main route table
- [ ] Outbound route table
- [ ] No rote table is associated with it

----

### 4. Frage

A best practice for IP addressing across multiple virtual private clouds (VPCs) is to use non-overlapping IP address ranges.

- [ ] True
- [ ] False


----

### 5. Frage

Which statements about a default VPC are true? (Select TWO.)

- [ ] A default VPC spans multiple Regions
- [ ] AWS creates a default VPC in each Region
- [ ] AWS creates a default VPC in each Availability Zone
- [ ] By default, each default VPC is available to one AWS account

----

### 6. Frage

What is a valid target in a route table?

- [ ] Elastic network interface ID
- [ ] Instance ID
- [ ] Virtual private gateway ID
- [ ] All of the above

----

### 7. Frage

A virtual private cloud (VPC) can span multiple Availability Zones

- [ ] True
- [ ] False

----

### 8. Frage

Which statements describe security groups? (Select TWO.)

- [ ] Multiple security groups can be assigned to an instance
- [ ] Security group associations can be added and modified after an instance is created
- [ ] Security groups are applied at the network's entry point
- [ ] Several instances can be assigned to a security group

----

### 9. Frage

A virtual private cloud (VPC) has an implicit router and a default route table that routes local traffic within it

- [ ] True
- [ ] False


----

### 10. Frage

What is a logical network segment in a virtual private cloud (VPC) that can only exist within a single Availability Zone?

- [ ] Customer gateway
- [ ] Internet gateway
- [ ] Security group
- [ ] Subnet
- [ ] Virtual private gateway



<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
