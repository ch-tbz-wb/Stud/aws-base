[10]: https://git-scm.com/downloads


Literatur
====

<br>

**AWS Academy:**

* [LearnerLab Services](./docs/AWS-Academy_LearnerLab-Educator_Guide.pdf) (Verfügbare Services)
* [Student Guide](./docs/AWS_Academy_LearnerLab-Student%20Guide.pdf) (Anleitung für Studenten)
* [Educator Guide](./docs/AWS-Academy_LearnerLab-Educator_Guide.pdf) (Anleitung für Coaches)



---

**Weiteres Thema:**
* [Thema TBD](xxx)
* [Thema TBD](xxx)

<br>

---

> [⇧ **Zurück zu Ressourcen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/cal_aws-account)

---