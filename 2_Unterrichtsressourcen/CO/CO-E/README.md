# Modul 5 - Computing, Containers and Serverless

## Folgende Topics werden in diesem Modul behandelt:

- AWS-Lambda
- APIs und REST
- Amazon API-Gateway
- Container auf AWS
- AWS Step-Funktionen


## Ziele dieses Moduls

- Du kannst den Zweck und die Funktion von AWS Lambda erklären
- Du kannst den Zweck und die Funktion von Anwendungsprogrammierschnittstellen (APIs), einschließlich RESTful-APIs erklären
- Du kennst die Vorteile und Funktionen von Amazon API Gateway
- Du kennst den Zweck und die Funktion von Containern und den AWS-Services, welche die Containernutzung unterstützen
- Du kannst den Zweck und die Funktion von AWS Step Functions erklären

* [Präsentation](./ACO-05-Computing-Containers-and-Serverless.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
