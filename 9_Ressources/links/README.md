[10]: https://git-scm.com/downloads


Links
====

<br>

**Dokumentation:**
* [AWS Online Dokumentation](https://docs.aws.amazon.com/)
* [AWS CLI Command Reference](https://docs.aws.amazon.com/cli/latest/index.html)



----

**AWS Academy:**
* [Entrypage](https://www.awsacademy.com/)

----

**AWS Online Learning:**
* [AWS Skillbuilder](https://explore.skillbuilder.aws/learn)
* [AWS Cloudquest](https://cloudquest.skillbuilder.aws) (Game)

----

**AWS Zertifizierungen:**
* [AWS Cloud Practitioner](https://aws.amazon.com/de/certification/certified-cloud-practitioner)
* [AWS Solutions Architect - Associate](https://aws.amazon.com/de/certification/certified-solutions-architect-associate)


---

**Cloud Computing:**
* [Wie funktioniert Cloud Computing](https://www.elektronik-kompendium.de/sites/com/1404051.htm) (Quelle: Elektronik Kompendium)

<br>

---

> [⇧ **Zurück zu Ressourcen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/cal_aws-account)

---