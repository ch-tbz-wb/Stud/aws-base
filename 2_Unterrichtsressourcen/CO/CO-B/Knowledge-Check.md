# Modul 2 - Knowledge Check

### 1. Frage

What type of AWS Systems Manager document can take a snapshot of an Amazon Elastic Compute Cloud (Amazon EC2) instance?

- [ ] Command document
- [ ] Run document
- [ ] Script document
- [x] Automation document _`(Ein "Automation Document kann allgemeine IT-Tasks auf AWS ausführen)`_

----

### 2. Frage

You have Bash scripts that you routinely run on your Amazon Elastic Compute Cloud (Amazon EC2) instance that run Linux. 
Which AWS service or AWS service feature provides the simplest way to automate the running of these scripts?

- [ ] Distributor
- [ ] AWS OpsWorks Stacks
- [ ] AWS CodePipeline
- [x] AWS Systems Manager 

_`(Mit AWS System Manager kann man Bash-Skriptbefehle in ein Befehlsdokument einfügen, das Systems Manager ausführen kann)`_

----

### 3. Frage

AWS CloudFormation templates can be written in either JavaScript Object Notation (JSON) or YAML Ain't Markup Language (YAML) files

- [x] True
- [ ] False

_`(CloudFormation supportet beide Typen)`_

----

### 4. Frage

Which of the follwing are AWS OpsWorks offerings? (Select THREE.)

- [x] AWS OpsWorks for Chef Automate
- [ ] AWS Tools for PowerShell
- [x] AWS OpsWorks for Puppet Enterprise
- [x] AWS OpsWorks Stacks


----

### 5. Frage

The URL for a website hosted in Amazon Simple Storage Service (Amazon S3) can include either a period (.) or a dash (-) to separate the website from the Region name.

- [x] True
- [ ] False 


----

### 6. Frage

AWS provides a single software development kit (SDK) that covers all programming languages

- [ ] True
- [x] False 

_`(Für jede von AWS unterstützte Programmiersprache gibt es ein separates SDK)`_

----

### 7. Frage

Which AWS Systems Manager feature displays operational data for AWS resources?

- [ ] AWS System Manager Agent
- [x] AWS Systems Manager Insights Dashboard
- [ ] AWS AppConfig
- [ ] None of the above

_`(AWS Systems manager Insights stellt zur Ansicht von "Operational Data" ein Dashboard zur Verfügung)`_

----

### 8. Frage

Which AWS Service can store configuration data?

- [x] Systems Manager Parameter Store
- [ ] AWS Secrets Manager
- [ ] Amazon Kinesis
- [ ] None of the above

----

### 9. Frage

You can create set time periods for running administrative tasks with AWS Systems Manager.

- [x] True
- [ ] False 


----

### 10. Frage

Which AWS Systems Manager feature enables access to an Amazon Elastic Compute Cloud (Amazon EC2) instance without opening inbound ports or bastion hosts?

- [ ] OpsCenter
- [ ] Explorer
- [x] Session Manager
- [ ] None of the above


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
