# Unterrichtsressourcen 

### Cloud Foundation (Einstieg)

* [Unterrichtsressource CF-0](CF/CF-0/): Modul 0 - Course Introduction
* [Unterrichtsressource CF-A](CF/CF-A/): Modul 1 - Cloud Concepts Overview
* [Unterrichtsressource CF-B](CF/CF-B/): Modul 2 - Cloud Economics and Billing (Selbststudium)
* [Unterrichtsressource CF-C](CF/CF-C/): Modul 3 - AWS Global Infrastructure Overview (Selbststudium)
* [Unterrichtsressource CF-D](CF/CF-D/): Modul 4 - AWS Cloud Security (Section 1 und 2, Lab 1 IAM)
* [Unterrichtsressource CF-E](CF/CF-E/): Modul 5 - Networking and Content Delivery (Section 1 - 3, Lab 2)
* [Unterrichtsressource CF-F](CF/CF-F/): Modul 6 - Compute (Section 1 - 3, Lab 3)
* [Unterrichtsressource CF-G](CF/CF-G/): Modul 7 - Storage
* [Unterrichtsressource CF-H](CF/CF-H/): Modul 8 - Databases
* [Unterrichtsressource CF-I](CF/CF-I/): Modul 9 - Cloud Architecture
* [Unterrichtsressource CF-J](CF/CF-J/): Modul 10 - Automatic Scaling and Monitoring


### Cloud Operations (Fortgeschritten)

* [Unterrichtsressource CO-0](CO/CO-0/): Modul 0 - Course Welcome and Overview
* [Unterrichtsressource CO-A](CO/CO-A/): Modul 1 - Understanding Systems Operations on AWS
* [Unterrichtsressource CO-B](CO/CO-B/): Modul 2 - Tooling and Automation
* [Unterrichtsressource CO-C](CO/CO-C): Modul 3 - Computing Servers
* [Unterrichtsressource CO-D](CO/CO-D/): Modul 4 - Computing, Scaling and Name Resolution
* [Unterrichtsressource CO-E](CO/CO-E/): Modul 5 - Computing, Containers and Serverless
* [Unterrichtsressource CO-F](CO/CO-F): Modul 6 - Computing Database Services
* [Unterrichtsressource CO-G](CO/CO-G): Modul 7 - Networking
* [Unterrichtsressource CO-H](CO/CO-H): Modul 8 - Storage and Archiving
* [Unterrichtsressource CO-I](CO/CO-I): Modul 9 - Monitoring and Security
* [Unterrichtsressource CO-J](CO/CO-J): Modul 10 - Managing Resource Consumption
* [Unterrichtsressource CO-K](CO/CO-K): Modul 11 - Creating Automated and Repeatable Deployments

<br>

---------

### ...Ergänzende Ressourcen für die private Nutzung
Hier sind noch weitere Ressourcen abgelegt, welche für Studenten geeignet sind, die sich später **ohne** AWS-Academy Account auf eine Zertifizierung vorbereiten wollen.
AWS bietet einen **FreeTIER-Account** an, der **ein Jahr lang** genutzt werden kann. Mit der Verwendung von **Mail-Aliases** kann jedes Jahr ein neuer FreeTIER-Account erstellt werden. Die Vorgehensweise wird unten erklärt.


#### AWS Account erstellen

* [AWS-Account erstellen](https://gitlab.com/ser-cal/cal_aws-account) _(Repository mit ausführlicher Anleitung. Inkl. Setup eines Google E-Mail-Aliases)_


---

#### AWS Account einrichten

![Video1:](images/Video.png) 5:10
[![Tutorial](images/aws-user.png)](https://web.microsoftstream.com/video/58b96b9e-2d8f-485d-8339-6507e3317ee3?list=studio)

---

#### AWS Billing Alarm einrichten

![Video2:](images/Video.png) 3:53
[![Tutorial](images/aws-billing-alarm.png)](https://web.microsoftstream.com/video/7ef8b980-b3bb-479e-9bdc-e6ac4f60a626?list=studio)


* [AWS Billing Alarm](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/monitor_estimated_charges_with_cloudwatch.html) _(Alarm einrichten, um frühzeitig informiert zu werden, wenn Kosten entstehen)_

---

### Webserver EC2 and Container

[Repository](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver) mit folgendem **Auftrag**

**AWS EC2 Webserver mit UserData aufsetzen**

![Video3:](images/Video.png) 13:25
[![Tutorial](images/aws-userdata.png)](https://web.microsoftstream.com/video/7a6c1d92-5883-4b03-86d1-834295e79651)
