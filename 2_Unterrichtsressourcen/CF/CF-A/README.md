# Modul 1 - Cloud Concepts Overview

## Folgende Topics werden in diesem Modul behandelt:

- Einführung in das Cloud-Computing
- Vorteile des Cloud-Computing
- Einführung in Amazon Web Services (AWS)
- AWS Cloud Adoption Framework (AWS CAF)

## Ziele dieses Moduls

- Du kannst verschiedene Arten von Cloud-Computing-Modellen definieren
- Du kannst die sechs Vorteile des Cloud Computing erklären
- Du bist in der Lage, die wichtigsten AWS-Servicekategorien und Kernservices zu erkennen
- Du kennst das AWS Cloud Adoption Framework (AWS CAF) - (Hilfestellungen und Best-Practices, um in die Cloud zu migrieren)

* [Präsentation](./AcademyCloudFoundations_Module_01.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
