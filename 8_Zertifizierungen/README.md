# Zertifizierungen


### AWS Certified Cloud Practitioner

- [Exam Guide (Deutsch)](/x_gitressourcen/Docs/AWS-Certified-Cloud-Practitioner_Exam-Guide_DEUTSCH.pdf)
- [Exam Guide (Englisch)](/x_gitressourcen/Docs/AWS-Certified-Cloud-Practitioner_Exam-Guide_ENGLISCH.pdf)

- [Prüfungsfragen (Deutsch)](/x_gitressourcen/Docs/AWS-Certified-Cloud-Practitioner_Sample-Questions_DEUTSCH.pdf)
- [Prüfungsfragen (Englisch)](/x_gitressourcen/Docs/AWS-Certified-Cloud-Practitioner_Sample-Questions_ENGLISCH.pdf)


*Siehe z.B. [https://www.digicomp.ch/zertifizierung](https://www.digicomp.ch/zertifizierung)*
