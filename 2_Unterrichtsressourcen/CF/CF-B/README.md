# Modul 2 - Cloud Economics and Billing (Selbststudium)

## Folgende Topics werden in diesem Modul behandelt:

- Grundlagen der Preisgestaltung
- Eigentumsgesamtkosten
- AWS-Organisationen
- AWS-Fakturierung und Kostenmanagement
- Technischer Support

## Ziele dieses Moduls

- Du verstehst die AWS-Preisphilosophie
- Du erkennst Grundlegende Preismerkmale
- Du kennst Elemente der Gesamtbetriebskosten
- Du kannst Ergebnisse des AWS-Preisrechners interpretieren
- Du weisst, wie man eine Organisationsstruktur einrichtet, welche die Rechnungsstellung und Kontotransparenz vereinfacht
- Du kannst die Funktionalität im AWS Billing Dashboard erklären
- Du verstehst die Verwendung von AWS Bills, AWS Cost Explorer, AWS Budgets, AWS Cost und Usage Reports
- Du kennst die verschiedenen Pläne und Funktionen des technischen AWS-Supports

* [Präsentation](./AcademyCloudFoundations_Module_02.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
