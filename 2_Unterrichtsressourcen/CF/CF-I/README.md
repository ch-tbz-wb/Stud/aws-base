# Modul 9 - Cloud Architecture

## Folgende Topics werden in diesem Modul behandelt:

- AWS Well-Architected Framework
- Zuverlässigkeit und hohe Verfügbarkeit
- AWS Trusted Advisor

## Ziele dieses Moduls

- Du kannst das AWS Well-Architected Framework beschreiben - einschließlich den sechs Säulen
- Du bist in der Lage, die Designprinzipien des AWS Well-Architected Frameworks zu erklären
- Du kennst die Bedeutung der Begriffe **Reliability** und **high avaliability** (Zuverlässigkeit und Hochverfügbarkeit)
- Du weisst, wie **AWS Trusted Advisor** die Kunden unterstützen kann
- Du kannst die AWS Trusted Advisor-Empfehlungen interpretieren und umsetzen

* [Präsentation](./AcademyCloudFoundations_Module_09.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
