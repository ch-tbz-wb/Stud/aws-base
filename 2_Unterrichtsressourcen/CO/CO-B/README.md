# Modul 2 - Tooling and Automation

## Folgende Topics werden in diesem Modul behandelt:

- AWS-Systemmanager
- Zusätzliche Verwaltungs- und Entwicklungstools
- Hosten einer statischen Website auf Amazon S3

## Ziele dieses Moduls

- Du kannst den Zweck und die Funktion von AWS Systems Manager und den zugehörigen Funktionen beschreiben
- Du kannst den Zweck und die Funktion von AWS Tools for PowerShell erklären
- Du kennst zusätzliche Entwicklungstools, die für Tools und Automatisierung verwendet werden, wie SDKs, AWS CloudFormation und AWS OpsWorks
- Du kannst erläutern, wie Amazon S3 zum Hosten einer statischen Website verwendet werden kann



* [Präsentation](./ACO-02-Tooling-and-Automation.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
