[10]: https://git-scm.com/downloads



Diverses
====

<br>

**Einführung GIT und Setup des Repositorys:**
* [Gitlab Repository](https://github.com/ser-cal/M300-Git-Repo-Setup) (Schritt für Schritt Anleitung)


<br>

---

> [⇧ **Zurück zu Ressourcen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/armindoerzbachtbz/cloud-native-bootcamp)

---