# Handlungskompetenzen

## Modulspezifische Handlungskompetenzen

 * B7 Technische Anforderungen analysieren und bestimmen
   * B7.1 Die ICT-Architektur zielorientiert (ICT Strategie) analysieren, beurteilen und bestimmen
   * B7.2 Geschäftsprozesse aus ICT-Sicht priorisieren, analysieren, spezifizieren und optimieren
   * B7.3 Technische Anforderungen aufnehmen und spezifizieren
   * B7.4 Den Einsatz von geschäftsrelevanten Systemen konzipieren
   * B7.5 Anforderungen für den Einsatz von Informatikmitteln spezifizieren
 * B12 System- und Netzwerkarchitektur bestimmen
   * B12.1 Die bestehende Systemarchitektur beurteilen und weiterentwickeln (Niveau: 3)
   * B12.3 Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln (Niveau: 3)
   * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)
 * B13 Konzepte und Services entwickeln
   * B13.2 Spezifische Testkonzepte erstellen und die Tests der relevanten Prüfobjekte planen
   * B13.3 Services gemäss Pflichtenheft planen
   * B13.4 Anforderungen aus dem Service-Management analysieren, entwickeln und integrieren
   * B13.5 Service-Levels unter Berücksichtigung der Servicestrategie und Kundenvorgaben entwickeln
 * B14 Konzepte und Services umsetzen
   * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten 
   * B14.2 Probleme und Fehler im operativen Betrieb überwachen, identifizieren, zuordnen, beheben oder falls erforderlich eskalieren
   * B14.5 ICT-Systeme und ICT-Dienstleistungen beschaffen
   * B14.6 Standardverträge für ICT-Lizenzen einsetzen
   * B14.7 Verrechnungsmodell operativ erstellen, umsetzen und ICT- Dienstleistungen budgetieren und verrechnen

## Allgemeine Handlungskompetenzen

* A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen 

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
