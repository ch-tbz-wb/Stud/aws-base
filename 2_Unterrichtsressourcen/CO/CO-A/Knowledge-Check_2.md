# Modul 1 - Knowledge Check

### 1. Frage

When you define a bucket in Anazon Simple Storage Service (Amazon S3), you must also specify the Region where the bucket will exist

- [ ] True
- [ ] False

----

### 2. Frage

Access control lists (ACLs) are defined with a JavaScript Object Notation (JSON) policy document

- [ ] True
- [ ] False

----

### 3. Frage

Which statement about policy rule evaluation for AWS Identity and Access Management (IAM) are correct? (Select THREE.)

- [ ] An explicit ALLOW overrides the default implicit denial of access to all resources, unless an explicit DENY overrides it.
- [ ] An explicit DENY does not override all ALLOWs
- [ ] All policies are evaluated before a request is allowed or denied.
- [ ] The evaluation order of the policies has no effect on outcome
- [ ] Results can either be allowed, denied, or submitted for further evaluation.

----

### 4. Frage

Access permissions define which actions a user can take and/or which resources a user can accss

- [ ] True
- [ ] False 


----

### 5. Frage

It is a best practice to follow the policy of granting least privilege when you assign permissions

- [ ] True
- [ ] False 

----

### 6. Frage

Which service grants customers permissions to centrally manage access to the launch, configuration management, and termination of a resource?

- [ ] AWS indentity and Access Management (IAM)
- [ ] Amazon Elastic Compute Cloud (Amazon EC2)
- [ ] Amazon Virtual Private Cloud (Amazon VPC)
- [ ] All of the above


----

### 7. Frage

Which AWS service features would be within the scope of a Region? (Select TWO.)

- [ ]	Amazon CloudFront distributions
- [ ]	AWS Identity and Access Management (IAM) users, groups, and roles
- [ ]	Amazon Machine Images (AMIs)
- [ ]	Amazon Route 53 hosted zones
- [ ]	Amazon Simple Storage Service (Amazon S3) bucket


----

### 8. Frage

Which of the following are best practices for IAM? (Select THREE.)

- [ ]	Delegate administration functions based on the principles of least privilege
- [ ]	Use the root account for daily administration
- [ ]	Enable multi-factor authentication
- [ ]	Rotate credentials regulary

----

### 9. Frage

Which output formats are supported by the AWS comand Line Interface (AWS CLI)? (Select THREE.)

- [ ]	ASCII-formatted table
- [ ]	Path specification
- [ ]	JavaScript Object Notation (JSON)
- [ ]	Tab-delimited text
- [ ]	YAML Ain't Markup Language (YAML)

----

### 10. Frage

Which part of the following AWS Command Line Interface (AWS CLI) Command specifies the operation to be performed?

`aws ec2 stop-incstances --instance-id i-1234567890abcdef0 --output json`

- [ ]	ec2
- [ ]	--instance-id i-1234567890abcdef0
- [ ]	stop-instances
- [ ]	YAML Ain't markup language
- [ ]	--output json


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
