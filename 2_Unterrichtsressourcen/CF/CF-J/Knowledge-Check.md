# Modul 10 - Knowledge Check

### 1. Frage

Which of the following AWS tools help your application scale up or down based on demand? (Choose two.)

- [ ] Availability Zones
- [x] Amazon EC2 Auto Scaling
- [ ] AWS CloudFormation
- [x] Elastic Load Balancer
- [ ] AWS Config

----

### 2. Frage

Which service would you use to send alerts based on Amazon CloudWatch alarms? (Select the best answer.)

- [x] Amazon Simple Notification Service (Amazon SNS)
- [ ] AWS CloudTrail
- [ ] AWS Trusted Advisor
- [ ] Amazon Route 53


----

### 3. Frage

Which of the following are characteristics of Amazon EC2 Auto Scaling? (Choose three.)

- [ ] Only supports dynamic scaling
- [x] Responds to changing conditions by adding or terminating instances
- [ ] Delivers push notifications
- [x] Launches instances from a specified Amazon Machine Image (AMI)
- [x] Enforces a minimum number of running Amazon EC2 instances


----

### 4. Frage

Which of the following must be configured on an Elastic Load balancing load balancer to expect incoming traffic? (Select the best answer.)

- [ ] A port
- [ ] A network interface
- [x] A listener
- [ ] An instance

----

### 5. Frage

Which of the following elements are used to create an Amazon EC2 Auto Scaling launch configuration? (Choose three.)

- [x] Amazon Machine Image (AMI)
- [ ] Load balancer 
- [x] Instance type
- [ ] Virtual private cloud (VPC) and subnets
- [x] Amazon Elastic Block Store (Amazon EBS) volumes

----

### 6. Frage

Which of the following services can help you collect important metrics from Amazon Relational Database Service (Amazon RDS) and Amazon Elastic Compute Cloud (Amazon EC2) instances? (Select the best answer.)

- [ ] Amazon CloudFront
- [ ] Amazon CloudSearch
- [x] Amazon CloudWatch
- [ ] AWS CloudTrail
- [ ] Amazon EC2 Auto Scaling

_`(Amazon CloudWatch ist ein Monitoring-Service für Ressourcen und Applikationen, welche in der AWS-Cloud laufen)`_

----

### 7. Frage

Which of the following elements are used to create an Amazon EC2 Auto Scaling launch configuration? (Choose three.)

- [x] Minimum size
- [ ] Health checks
- [x] Desired capacity
- [x] Maximum size

----

### 8. Frage

There is an audit at your company and they need to have a log of all access to AWS resources in the account. Which of the following services can assist in providing these details? (Select the best answer.)

- [ ] Amazon CloudWatch
- [x] AWS CloudTrail
- [ ] Amazon Elastic Compute Cloud (Amazon EC2)
- [ ] Amazon Simple Notification Service (Amazon SNS)

_`(Amazon CloudTrail ist ein Service, der Governance, Compliance, Betriebs- und Risikoprüfung eines AWS-Kontos ermöglicht)`_

----

### 9. Frage

In Elastic Load Balancing, when the load balancer detects an unhealthy target, which of the following are true? (Choose three.)

- [x] Stops routing traffic to that target
- [ ] Triggers an alarm
- [x] Resumes routing traffic when it detects that the target is healthy again
- [ ] Resumes routing traffic when manually restarted
- [x] Routes traffic to a healthy target


----

### 10. Frage

What are the three types of load balancers that Elastic Laod Balancing offers?

- [ ] Internet Load Balancer
- [x] Application Load Balancer
- [x] Network Load Balancer
- [ ] Compute Load Balancer
- [x] Classic Load Balancer
- [ ] Auto Scaling Load Balancer


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
