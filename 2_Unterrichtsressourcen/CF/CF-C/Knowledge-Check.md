# Modul 3 - Knowledge Check

### 1. Frage

Which component of the AWS Global Infrastructure does Amazon CloudFront use to ensure low-latency delivery? (Select the best answer.)

- [ ] AWS Regions
- [x] AWS edge locations
- [ ] AWS Availability Zones
- [ ] Amazon Virtual Private Cloud (Amazon VPC) 

----

### 2. Frage

You can run applications and workloads from a Region closer to the end users to ________ latency.

- [ ] increase
- [x] decrease


----

### 3. Frage

True or false? Networking, storage, compute, and database are examples of service categories that AWS offers

- [x] True
- [ ] False 

----

### 4. Frage

Which of the following are geographic areas that host two or more Availability Zones? (Select the best answer.)

- [ ] AWS Origins
- [x] AWS Regions
- [ ] Compute zones
- [ ] Edge locations

----

### 5. Frage

_____ means the infrastructure has built-in component redundancy and _____ means that resources dynamically adjust to increases or decreases in capacity requirements

- [ ] No human intervention, fault tolerant
- [ ] Elastic and scalable, no human intervention
- [x] Fault tolerant, elastic and scalable
- [ ] Fault tolerant, no human intervention
- [ ] Elastic and scalable, fault tolerant


----

### 6. Frage

True or false? Availability Zones within a Region are connected through low-latency links.

- [x] True
- [ ] False 

----

### 7. Frage

Which of these statements about Availability Zones ist not true? (Select the best answer)

- [ ] Availability Zones are designed for fault isolation
- [ ] Availability Zones are made up of one or more data centers
- [x] A data center can be used for more than one Availability Zone
- [ ] Availability Zones are connected to each other using high-speed private links


----

### 8. Frage

What is true about regions? (Choose two.)

- [ ] All Regions are located in one specific geographic area
- [x] A Region is a physical location that has multiple Availability Zones
- [x] Each Region is located in a separate geographic area
- [ ] They are the pysical locations of your customers

----

### 9. Frage

AWS highly recommends provisioning your compute resources across ____ Availability Zones. (Select the best answer.)

- [ ] single
- [x] multiple
- [ ] all
- [ ] no

----

### 10. Frage

True or false? Edge locations are only located in the same general area as Regions.

- [ ] True
- [x] False




<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
