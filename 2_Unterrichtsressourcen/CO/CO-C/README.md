# Modul 3 - Computing Servers

## Folgende Topics werden in diesem Modul behandelt:

- Computing auf AWS
- Verwalten von AWS-Instanzen
- Sicherung von AWS-Instanzen
- Preise für Amazon EC2-Instances


## Ziele dieses Moduls

- Du kannst den Dienst **Amazon Elastic Compute Cloud** (Amazon EC2) beschreiben
- Du kennst den Unterschied zwischen den verschiedenen Instance-Types und Speicheroptionen, die für EC2-Instanzen verfügbar sind
- Du weisst, welche Netzwerkkomponenten angegeben werden müssen, wenn du eine EC2-Instanz startest
- Du kannst den Unterschied zwischen Amazon EC2-Benutzerdaten und -Metadaten erklären
- Du kennst die unterschiedlichen Lebenszykluszustände einer EC2-Instanz
- Du bist in der Lage, das Modell der geteilten Verantwortung zu erklären
- Du kannst Amazon EC2-Instanzen erstellen



* [Präsentation](./ACO-03-Computing-Servers.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
