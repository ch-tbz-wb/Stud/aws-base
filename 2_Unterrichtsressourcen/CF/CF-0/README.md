# Cloud Foundation (für Einsteiger) - Course Introduction

## Nach Abschluss dieses Moduls kennst Du:

- den Zweck des AWS Academy Cloud Foundations-Kurses
- die Kursstruktur
- den AWS-Zertifizierungsprozess
- die AWS-Dokumentationswebseiten

## Ziele des Cloud Foundation-Kurses

- Definition der AWS Cloud
- AWS-Preisphilosophie.
- Globale Infrastrukturkomponenten von AWS.
- Identify the global infrastructure components of AWS.
- Sicherheits- und Compliance-Maßnahmen der AWS Cloud, einschließlich AWS Identity and Access Management (IAM).Create an AWS Virtual Private Cloud (Amazon VPC).
- Amazon Elastic Compute Cloud (EC2), AWS Lambda und AWS Elastic Beanstalk.
- Unterschiede zwischen Amazon S3, Amazon EBS, Amazon EFS und Amazon S3 Glacier.
- Verwendung der unterschiedlichen AWS Database-Services, einschließlich Amazon Relational Database Service (RDS), Amazon DynamoDB, Amazon Redshift und Amazon Aurora.
- Prinzipien der AWS Cloud-Architektur.
- Schlüsselkonzepte im Zusammenhang mit Elastic Load Balancing (ELB), Amazon CloudWatch und Auto Scaling

* [Präsentation](AcademyCloudFoundations_Module_00.pdf)


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
