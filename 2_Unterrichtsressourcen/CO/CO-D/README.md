# Modul 4 - Computing, Scaling and Name Resolution

## Folgende Topics werden in diesem Modul behandelt:

- Elastic Load Balancing (ELB)
- Amazon EC2 Auto Scaling (including
prediction challenge)
- Amazon Route 53


## Ziele dieses Moduls

- Du kannst die Elastic Load Balancing-Funktionen beschreiben
- Du kennst die Unterschiede zwischen den verschiedenen ELB-Load-Balancern
- Du kannst Amazon EC2 Auto Scaling beschreiben und kennst die Startkonfigurationen
- Du kannst EC2 Auto Scaling in AWS anwenden
- Du kannst die Funktionen und Routing-Optionen von Amazon Route 53 erklären
- Du bist in der Lage, ein Failover-Routing zu konfigurieren


* [Präsentation](./ACO-04-Computing-Scaling-and-Name-Resolution.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
