# Modul 11 - Creating Automated and Repeatable Deployments

## Folgende Topics werden in diesem Modul behandelt:

- Konfigurationsmanagement in der Cloud
- Verwendung von Konfigurationssoftware
- Erstellen einer AMI-Building-Strategie
- AWS CloudFormation
- Fehlerbehebung bei AWS CloudFormation-Vorlagen
- CI/CD-Bereitstellung auf AWS


## Ziele dieses Moduls

- Du kannst einige der AWS-Services für das Konfigurationsmanagement aufzählen
- Du kennst die Herausforderungen im Zusammenhang mit Cloud-Bereitstellungen und mögliche Lösungen zur Bewältigung dieser Herausforderungen
- Du kennst den Begriff **Infrastruktur als Code (IaC)** und den Mehrwert, den IaC schafft
- Du kannst den Zweck von AWS CloudFormation beschreiben
- Du kennst verschiedene Fehlerarten beim Einsatz von AWS CloudFormation und weisst, wie diese Fehler behoben werden können
- Du kennst die Best Practices für die Verwendung von AWS CloudFormation
- Du kennst die Prozesse zur Fehlerbehebung bei AWS CloudFormation 
- Du bist in der Lage die Continuous Integration \Continuous Delivery (CI\CD)-Bereitstellung auf AWS zu nutzen


* [Präsentation](./ACO-11-Creating-Automated-and-Repeatable-Deployments-1.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
