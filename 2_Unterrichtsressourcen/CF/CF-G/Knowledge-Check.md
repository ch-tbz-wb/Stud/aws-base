# Modul 7 - Knowledge Check

### 1. Frage

True or False? Amazon Simple Storage Service (Amazon S3) is an object storage suitable for the storage of flat files like Microsoft Word documents, photos, etc...

- [x] True
- [ ] False

----

### 2. Frage

Amazon S3 replicates all objects ____. (Select the best answer.)

- [ ] on multiple volumes within an Availability Zone
- [x] in multiple Availability Zones within the same Region
- [ ] across multiple Regions for higher durability
- [ ] on multiple S3 buckets

----

### 3. Frage

Which of the following can be used as a storage class for an S3 object lifecycle policy? (Choose three)

- [x] S3 - Standard Access
- [ ] AWS Storage Gateway
- [x] S3- Infrequent Access
- [x] Simple Storage Service Glacier
- [ ] S3 - Reduced Redundancy Storage
- [ ] Amazon Dynamo DB

----

### 4. Frage

The name of an S3 bucket must be unique ______.  (Select the best answer.)

- [x] worldwide across all AWS accounts
- [ ] within a Region
- [ ] across all your AWS accounts
- [ ] within your AWS account

----

### 5. Frage

You can use Amazon Elastic File System (Amazon EFS) to: (Select the best answer.)

- [ ] provide simple, scalable, elastic file storage for use only within AWS
- [x] implement storage for Amazon EC2 instances that multiple virtual machines can access at the same time
- [ ] host a robust CDN to deliver entire web sites with dynamic static, and streaming content
- [ ] generate user-specific content


----

### 6. Frage

Amazon Elastic Block Store (Amazon EBS) is recommended when data _____ and _____. (Choose two)

- [ ] requires object-level storage
- [x] must be quickly accessible, requirin long-term persistence
- [x] requires an encryption solution
- [ ] needs to be stored in a different Availability Zone than the one the EC2 instance is in



----

### 7. Frage

True or False? By default, all data stored in Amazon S3 is viewable by the public

- [ ] True
- [x] False

----

### 8. Frage

Regarding Amazon S3 Glacier, what is a Vault? (Select the best answer.)

- [ ] The rules that determine who may (or may not) access archives
- [ ] An object (photos, videos, files, or documents)
- [x] A container for storing archives
- [ ] A policy that identifies who can access content stored in Glacier


----

### 9. Frage

True or False? When you create a bucket in Amazoon S3, it is associated with a specifig AWS Region

- [x] True
- [ ] False


----

### 10. Frage

Which of the following are features of Amazon Elastic Block Store (Amazon EBS)? (Choose two)

- [ ] Amazon EBS data is automatically backed up to tape
- [ ] Data on an Amazon EBS volume is lost when the attached instance is stopped
- [x] Amazon EBS volumes can be encrypted transparently to workloads on the attached instance
- [x] Data stored on Amazon EBS is automatically replicated within an Availability Zone


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
