# Modul 10 - Managing Resource Consumption

## Folgende Topics werden in diesem Modul behandelt:

- Tagging
- Cost management tools and best practices
- AWS Trusted Advisor


## Ziele dieses Moduls

- Sie können den Zweck und die Funktion des Tagging in AWS erklären
- Sie können die mit dem Tagging verbundenen Kostenmanagementstrategien erklären
- Sie kennen die Kostenvorteile der Cloud
- Sie können den Zweck und die Funktion des AWS Trusted Advisor-Services erklären
- Sie können Ressourcen mit "Tagging" verwalten


* [Präsentation](./ACO-10-Managing-Resource-Consumption.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
