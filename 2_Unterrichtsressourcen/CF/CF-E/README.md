# Modul 5 - Networking and Content Delivery

## Folgende Topics werden in diesem Modul behandelt:

- Networking basics
- Amazon VPC
- VPC networking
- VPC security
- Amazon Route 53
- Amazon CloudFront

## Ziele dieses Moduls

- Du kennst die AWS Netzwerkgrundlagen
- Du weisst, wie ein virtuelles Netzwerk in der Cloud (VPC) aufgebaut ist und funktioniert
- Du kannst ein Netzwerkdiagramm lesen
- Du bist in der Lage, eine grundlegenden VPC-Architektur zu entwerfen
- Du kennst die Schritte zum Erstellen einer VPC
- Du kannst Sicherheitsgruppen identifizieren
- Du kannst eine eigene VPC erstellen und zusätzliche Komponenten hinzufügen
- Du kennst die Grundlagen von Amazon Route 53
- Du kennst die Vorteile von Amazon CloudFront

* [Präsentation](./AcademyCloudFoundations_Module_05.pdf)
* [Knowledge-Check](./Knowledge-Check_2.md) - _(10 Fragen zu diesem Modul)_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
