# Handlungsziele und Handlungssituationen 

Dieser Kurs bietet einen Einstieg in das weitreichende Thema **Cloud Computing**. Wir werden neben der Theorie auch praxisorientierte Aufträge durchführen. 

Es gibt viele Gründe, warum es nützlich ist, sich Know-how im Bereich Cloud Computing zu erwerben. Aus der Perspektive eines **Studenten** stechen dabei folgende heraus:

* **Steigende Nachfrage nach Cloud-Fähigkeiten:** Die Nachfrage nach Fähigkeiten im Bereich Cloud Computing steigt stetig, da immer mehr Unternehmen auf Cloud-Lösungen setzen.

* **Wettbewerbsvorteil:** Mit einem fundierten Verständnis für Cloud Computing kannst Du dich von anderen IT-Profis abheben und einen Wettbewerbsvorteil im Job erlangen.

* **Karrieremöglichkeiten:** Ein tiefes Verständnis für Cloud Computing kann Dir neue Karrieremöglichkeiten eröffnen, da es eine wachsende Nachfrage nach Fachleuten gibt, die in der Lage sind, Cloud-Lösungen zu implementieren und zu verwalten.

* **Zugang zu fortschrittlicher Technologie:** Cloud Computing ermöglicht es Unternehmen, auf fortschrittliche Technologien zuzugreifen, die sie sich sonst vielleicht nicht leisten könnten. Du gehörst mit Deinem Know-how zu den Fachleuten, welche diese Firmen bei den Migrationen unterstützen.


---

## Handlungssituationen
...welche im Kurs erlernt werden: 

### 1. AWS Cloud-Konzepte

Ich habe einen Überblick über die wichtigsten AWS Cloud-Konzepte, einschließlich ihrer Vor- und Nachteile bzgl. On-Prem Lösungen.  

### 2. Shared Responsibility Model

Ich kenne das «Shared Responsibility Model», und weiss Bescheid über meine Pflichten, die ich bei AWS zu erfüllen habe. 

### 3. AWS-Dienste kennen
Ich kenne die Funktionen und Anwendungsbereiche der wichtigsten Dienste von AWS, wie EC2, S3, VPC und IAM.

### 4. Anwendungen in der AWS-Cloud
Ich kann verschiedene Dienste in der AWS-Cloud aufsetzen.

### 5. Kostenmanagement
Ich weiss, wie und wann Kosten bei genutzten Services entstehen und kann diese überwachen und verwalten.

### 6. Zertifizierung
Ich weiss, in welcher Form und in welchem Umfang die Einstiegszertifizierung **AWS Cloud Practitioner** durchgeführt wird und wie ich mich darauf vorbereite. 
