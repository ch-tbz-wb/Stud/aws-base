# Modul 11 - Knowledge Check

### 1. Frage

If a user cannot create a resource, you should check if the user has the permissions to create it

- [x] True
- [ ] False


----

### 2. Frage

Which type of AWS CloudFormation function can be used to refer to a value in a comma-delimited list?

- [ ] CloudFormation::Init
- [x] Fn::Select
- [ ] Ref
- [ ] Fn::Get Att


----

### 3. Frage

Where is the source of the error if a WaitCondition times out or returns an error?

- [x] AWS::CloudFormation::Init
- [ ] Fn::Select
- [ ] Ref
- [ ] All of the above


----

### 4. Frage

Which AWS service enables you to build a script-like template that represents a stack of AWS resources, and that can be used to launch precisely defined environments on AWS?

- [ ] Amazon Elastic Compute Cloud (Amazon EC2)
- [x] AWS CloudFormation
- [ ] Custom Amazon Machine Images (AMIs)
- [ ] All of the above


----

### 5. Frage

When you create a custom Amazon Machine Image (AMI), it is only available in the Region in which it was created

- [x] True
- [ ] False

----

### 6. Frage

Which resource does Amazon EC2 Auto Scaling use?

- [ ] Automation documents
- [ ] Git repositories
- [x] Launch templates
- [ ] Dynamic healing policies

----

### 7. Frage

What can an AWS CloudFormation template contain? (Select TWO.)

- [ ] Stacks
- [x] Code that describes AWS resources
- [ ] AWS resources
- [x] AWS Command Line Interface (AWS CLI) commands

----

### 8. Frage

JavaScript Object Notation (JSON) documents do not support binary data

- [x] True
- [ ] False


----

### 9. Frage

Which AWS CloudFormation resource type can provide metadata about an Amazon Elastic Compute Cloud (Amazon EC2) instance for the cfn-init helper script?

- [ ] WaitCondition
- [x] Init
- [ ] Resource section of the AWS CloudFormation template
- [ ] Parameters

----

### 10. Frage

There is no repository size limit for AWS CodeCommit

- [x] True
- [ ] False


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
