# Umsetzung
 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 1

## Lektionen

* Präsenz: 40
* Virtuell: 0
* Selbststudium: 20

## Lernziele
 - Verstehen und Umsetzung von Cloud-Konzepten und Beurteilung der Wirtschaftlichkeit
   - Funktionsumfang und Angebot AWS Cloud kennenlernen und mit anderen Anbietern vergleichen
   - Beurteilung der Wirtschaftlichkeit: 
     - Rolle der Betriebskosten (OpEx), Rolle von Kapitalausgaben (CapEx), Arbeitskosten im Zusammenhang mit dem lokalen Betrieb, Auswirkungen der Softwarelizenzierungskosten bei der Migration in die Cloud
     - Kostensenkungspotential der Cloud beurteilen
   - Gegenüberstellung verschiedener Preismodelle (On-demand, Reserved, Spot)
   - Kostenstrukturen und Abrechnung von Cloud-Services
   - Laufende Kostenüberwachung und -analyse
 - Umsetzung von Datensicherheit in der Cloud
   - Cloud-Architekturdesigns: Ausfallsicherheit, Entkopplung der Komponenten, Implementierung von Elastizität
   - Definieren von Sicherheits- und Compliance-Konzepten der AWS Cloud
   - Verwaltung der Zugriffsrechte am Beispiel von AWS Access Management-Funktionen
   - Identifizieren von Ressourcen für die Sicherheitsunterstützung 
 - Wichtige Technologien kennenlernen, deren Einsatzzweck bestimmen und Bereitstellungsvorgänge kennen
   - Definieren von Methoden zur Bereitstellung und zum Betrieb in der AWS Cloud (APIs, SDK, Management Console, CLI, IaaS)
   - Definieren der globalen AWS-Infrastruktur (Regionale Verfügbarkeit, Hochverfügbarkeit)
   - Typische Cloud-Services am Beispiel der wichtigsten AWS-Services
   - Ressourcen und Unterstützungsmöglichkeiten bei AWS zu identifizieren, einschließlich der Nutzung von Dokumentationen, dem Verständnis der verschiedenen Support-Ebenen, der Kenntnis des Partner-Netzwerks, der Quellen für technische Unterstützung und Wissen sowie der Vorteile des Einsatzes des AWS Trusted Advisor.

## Voraussetzungen:

Vertrautheit mit Cloud-Computing-Konzepten, Praktische Kenntnisse von verteilten Systemen, Vertrautheit mit allgemeinen Netzwerkkonzepten

## Dispensation

Gemäss Reglement HF Lehrgang 

Eine Dispensation des Moduls ist möglich, wenn vorher sämtliche Module des Online-Kurses **AWS Foundation** im HF Vorbereitungskurs durchgearbeitet - und die Fragen mit **mind. 80% richtig** beantwortet - wurden.

## Methoden

Blended Learning, Self-Learning Plattformen und praktische Laborübungen, Coaching durch Lehrperson

## Technologien

AWS Academy-Lab, AWS Management Console, S3, EBS, VPC, VPN, EC2, RDS, DynamoDB, Redshift, Ansible, Linux, CLI, Git

## Schlüsselbegriffe

Cloud Computing, Iaas, PaaS, Elastizität, Cloud, Cloud-Provider, Deployment, AWS, On-Demand, APIs, SDKs

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshops, Coaching, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel 
- [AWS Academy](https://www.awsacademy.com)




