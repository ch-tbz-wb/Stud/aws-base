# Modul 8 - Knowledge Check

### 1. Frage

You are designing an e-commerce web application that will scale to hundreds of thousands of concurrent users. Which database technology is best suited to hold the session state in this example?

- [ ] Amazon Relational Database Service (Amazon RDS)
- [ ] Amazon Dynamo DB
- [ ] Amazon Redshift
- [ ] Amazon Simple Storage Service (Amazon S3)

----

### 2. Frage

You need to find an item in an Amazon DynamoDB table using an attribute other than the item's primary key. Which of the following operations should you use? (Select the best answer.)

- [ ] Put Item
- [ ] Scan
- [ ] Query
- [ ] Get Item


----

### 3. Frage

In Amazon DynamoDB, what does the query operation enable you to do? (Select the best answer.)

- [ ] Query a table using the partition key and an optional sort key filter
- [ ] Query any secondary indexes that exist for a table
- [ ] Efficiently retrieve items from a table or secondary index
- [ ] All of the above


----

### 4. Frage

Which AWS Cloud service is best suited for analyzing your data by using standard structured query language (SQL) and your existing business intelligence (BI) tools?  (Select the best answer.)

- [ ] Amazon Relational Database Service (Amazon RDS)
- [ ] Amazon Simple Storage Service Glacier
- [ ] Amazon DynamoDB
- [ ] Amazon Redshift

----

### 5. Frage

In Amazon DynamoDB, an attribute is _____. (Select the best answer.)

- [ ] a fundamental data element
- [ ] a collection of items
- [ ] a collection of attributes


----

### 6. Frage

If you are developing an application that requires a database with extremly fast performance, fast scalability, and flexibility in the database schema, which service should you consider? (Select the best answer.)

- [ ] Amazon Relational Database Service (Amazon RDS)
- [ ] Amazon ElastiCache
- [ ] Amazon DynamoDB
- [ ] Amazon Redshift


----

### 7. Frage

Which of the following use cases is appropriate for using Amazon Relational Database Service (Amazon RDS)?  (Select the best answer.)

- [ ] Massive read/write rates
- [ ] Simple GET or PUT requests
- [ ] Complex transactions
- [ ] All of the above

----

### 8. Frage

A company has an application, which consists of a .NET layer that connects to a MySQL database. They want to move this application on to AWS and use AWS features such as high availability and automated backups. Which of the following would be an ideal database for this use case? (Select the best answer.)

- [ ] Amazon DynamoDB
- [ ] Amazon Aurora
- [ ] Amazon RDS
- [ ] Amazon Redshift


----

### 9. Frage

True or False? Amazon RDS automatically patches the database software and backs up your database, storing the backups for a user-defined retention period and enabling point-in-time recovery

- [ ] True
- [ ] False


----

### 10. Frage

What should you consider when choosing a database type? (Select the best answer.)

- [ ] Data size
- [ ] Data access period
- [ ] Query frequency
- [ ] Highly available
- [ ] All of the above


<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
