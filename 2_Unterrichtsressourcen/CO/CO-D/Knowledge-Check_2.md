# Modul 4 - Knowledge Check

### 1. Frage

What is the primary purpose of automatic scaling?

- [ ] To ensure the long-term reliability of a particular virtual resource
- [ ] To ensure that applications are automatically adjusted for capacity to maintain steady, predictable performance at the lowest possible cost
- [ ] To orchestrate the use of multiple parallel resources to direct incoming user requests
- [ ] To ensure the long-term reliability of a particular physical resource


----

### 2. Frage

Which AWS service provides customers with a cost-effective way to route end users to Amazon Elastic Cloud (Amazon EC2) instances and Elastic Load Balancing load balancers?

- [ ] Amazon CloudFront
- [ ] Amazon CloudWatch
- [ ] Amazon Route 53
- [ ] Amazon Virtual Private Cloud


----

### 3. Frage

What could be a final recommended (but optional) step when you create a load balancer?

- [ ] Use the create-load-balancer command, and then specify two subnets from different Availability Zones
- [ ] Use the create-target-group command to create the target groups, and then specify the virtual private cloud (VPC) that was used in the Amazon Elastic Compute Cloud (Amazon EC2) instances
- [ ] Use the describe-target-health command to verify the health of the instances for any registered instances
- [ ] Use the register-targets command to register the instances within the target group

----

### 4. Frage

What happens to CPU utilization when an Amazon Elastic Compute Cloud (Amazon EC2) Auto Scaling group cannot continue scaling up?

- [ ] Decreases
- [ ] Increases
- [ ] Stabilizes
- [ ] Terminates


----

### 5. Frage

Application Load Balancers are not supported by applications that run in containers

- [ ] True
- [ ] False

----

### 6. Frage

A health check is completed for all targets that are registered to a target group, as specified ty the load balancer's listener rule

- [ ] True
- [ ] False


----

### 7. Frage

Which statements are related to how an Amazon Elastic Compute Cloud (Amazon EC2) scaling policy can be triggered? (Select FOUR.)

- [ ] Amazon CloudWatch-driven
- [ ] Cooldown Period Completed
- [ ] Instance Failure via Health Check
- [ ] Manually Triggered
- [ ] Scheduled Action

----

### 8. Frage

Creating alarms that are only triggered by sustained state changes can reduce thrashing. (Thrashing is the condition where there is excessive use of a computer's virtual memory, and the computer is no longer able to service the resource needs of applications that run on it)

- [ ] True
- [ ] False


----

### 9. Frage

Which type of load balancer supports content-based routing?

- [ ] Application Load Balancer
- [ ] Classic Load Balancer
- [ ] Network Load Balancer
- [ ] All of these
- [ ] None of these

----

### 10. Frage

Which routing policy routes traffic to several resources, based on established proportions?

- [ ] Failover routing policy
- [ ] Geoproximity routing policy
- [ ] Multi-value answer routing policy
- [ ] Simple routing policy
- [ ] Weighted routing policy



<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
