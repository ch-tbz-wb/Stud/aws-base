# Modul 5 - Knowledge Check

### 1. Frage

Which AWS services can increase the ease of running and managing Docker containers? (Select THREE.)

- [x] Amazon Elastic Container Service (Amazon ECS)
- [x] Amazon Elastic Kubernetes Service (Amazon EKS)
- [x] AWS Fargate
- [ ] Amazon Elastic Compute Cloud (Amazon EC2)


----

### 2. Frage

AWS Lambda automatically scales to support incoming requests without additional configuration, and it remains fault-tolerant

- [x] True
- [ ] False


----

### 3. Frage

What performs the work in a workflow when you use AWS Step Functions?

- [ ] States
- [x] Tasks
- [ ] Transitions
- [ ] All of these

----

### 4. Frage

Which AWS service should a customer select if they must store, manage, and deploy Docker container images, with considerations for encryption, third-party integrations, and high availability?

- [ ] Amazon Elastic Kubernetes Service (Amazon EKS)
- [x] Amazon Elastic Container Registry (Amazon ECR)
- [ ] AWS Fargate
- [ ] Amazon Lightsail


----

### 5. Frage

Which of the following provides a managed service for running containers?

- [ ] Amazon Container Service (Amazon ECS)
- [x] AWS Fargate
- [ ] Amazon Elastic Container Registry (Amazon ECR)
- [ ] All of the above

----

### 6. Frage

A trucking company wants to minimize dependence on their cloud service provider, while increasing the development speed of various applications. The company is deciding if they should use containers or go serverless. They need secure compute capacity when trucks are at their home base. They must also support partner companies when the partner companies are visiting them, and they might need to accommodate those users once every couple of months. Which statements describe when the company might find that containers are a more ideal option? (Select THREE.)

- [ ] Have the ability to scale back or tear down as needed
- [x] They want to manage the resources and their respective policies, and oversee security 
- [ ] They must ensure that their cloud service provider provisions the infrastructure and manages capacity
- [x] They must move software from one environment to another through isolation
- [x] They have requirements for portability and not getting locked into a single vendor

----

### 7. Frage

Which AWS service feature provides encryption for data at rest for Amazon Elastic Container Registry (Amazon ECR)?

- [ ] Amazon API Gateway
- [ ] Amazon Simple Storage Service (Amazon S3) client-side encryption
- [x] Amazon Simple Storage Service (Amazon S3) server-side encryption
- [ ] Amazon Simple Storage Service Glacier

----

### 8. Frage

Amazon Elastic Container Registry (Amazon ECR) uses defined policies with AWS Identity and Access Management (IAM) to control who has access to container images

- [x] True
- [ ] False


----

### 9. Frage

Amazon Elastic Kubernetes Services (Amazon EKS) deploys a cluster's worker nodes across multiple Regions to eliminate a single point of failure

- [ ] True
- [x] False

_`(Korrekt ist: EKS deploys a cluster's worker nodes across multiple Availability Zones - NICHT Regions)`_

----

### 10. Frage

Which statements describe how Kubernetes works? (select THREE.)

- [ ] Kubernetes gives each cluster their own IP address and a single Domain Name System (DNS) name that is used for external traffic
- [x] Kubernetes schedules containers to run on the cluster based on the available compute resources and the resource requirements of each container
- [x] Kubernetes manages a cluster of compute instances
- [x] A pod is automatically started or restarted if the pod is not running or fails



<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
