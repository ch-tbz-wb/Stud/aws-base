# Modul 5 - Knowledge Check

### 1. Frage

With Amazon Virtual Private Cloud (Amazon VPC), what is the smallest size subnet you can have in a VPC? (Select the best answer.)

- [ ] /26
- [ ] /24
- [x] /28
- [ ] /30

----

### 2. Frage

With Amazon Virtual Private Cloud (Amazon VPC), what is the maximum size IP address range you can have in a VPC? (Select the best answer.)

- [ ] /28
- [ ] /30
- [ ] /24
- [x] /16

----

### 3. Frage

You need to allow resources in a private subnet to access the internet. Which of the following must be present to enable this access? (Select the best answer.)

- [ ] Security groups
- [ ] Route tables
- [x] NAT gateways
- [ ] Network access control lists

----

### 4. Frage

Which AWS networking service enables a company to create a virtual network within AWS? (Select the best answer.)

- [ ] AWS Config
- [ ] AWS Direct Connect
- [ ] Amazon Route 53
- [x] Amazon Virtual Private Cloud (Amazon VPC)

----

### 5. Frage

True or false? Private subnets have direct access to the internet

- [ ] True
- [x] False 


----

### 6. Frage

Which component of AWS Global Infrastructure does Amazon CloudFront use to ensure low-latency delivey? (Select the best answer)

- [ ] Amazon Virtual Private Cloud (Amazon VPC)
- [ ] AWS Availability Zones
- [x] AWS edge locations
- [ ] AWS Regions

----

### 7. Frage

Which of the following is an optional security control that can be applied at the subnet layer of a VPC? (Select the best answer)

- [ ] Firewall
- [ ] Security group
- [x] Network ACL
- [ ] Web application firewall

----

### 8. Frage

What happens when you use Amazon Virtual Private Cloud (Amazon VPC) to create a new VPC? (Select the best answer)

- [ ] Three subnets are created by default: one for each Availability Zone
- [x] A main route table is created by default
- [ ] Three subnets are created by default in one Availability Zone
- [ ] An internet gateway is created by default

----

### 9. Frage

Which of the following can be used to protect Amazon Elastic Compute Cloud (Amazon EC2) instances hosted in AWS? (Select the best answer.)

- [x] Security group
- [ ] All of the above
- [ ] Internet Gateway
- [ ] AMI


----

### 10. Frage

You are a solutions architect who works at a large retail company that is migrating its existing infrastructure to AWS. You recommend that they use a custom VPC. When you create a VPC, you assign it to an IPv4 Classless Inter-Domain Routing (CIDR) block of 10.0.1.0 /24 (which has 256 total IP addresses). How many IP addresses are available? (Select the best answer.)

- [ ] 256
- [ ] 246
- [ ] 250
- [x] 251

_`(Bei AWS sind per Default 5 IP-Adressen reserviert)`_

<br>

---

> [⇧ **Zurück zu Unterrichtsressourcen**](../../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-wb/TE/modules/aws/aws-base)

---
